#!/usr/bin/env bash
curl --trace billing -H "Content-Type: application/json"  -H "Authorization: Bearer 68yG0rfXbJXkmuA6VAYGrs7X2vRLcY" -X POST 'http://localhost:8000/api/v1/profile/billing/' \
    -d '{"first_name":"Ivan","last_name":"Ivanov","patronymic":"Ivanovich","country":1,"city":"Almaty","address":"Lenina, 33","email":"fake@fake.com","phone":"234234234234","zip":"2345555"}'