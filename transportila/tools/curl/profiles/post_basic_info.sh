#!/usr/bin/env bash
curl --trace basicinfo -H "Content-Type: application/json"  -H "Authorization: Bearer 68yG0rfXbJXkmuA6VAYGrs7X2vRLcY"  -X POST 'http://localhost:8000/api/v1/profile/basic/' \
    -d '{"first_name":"Ivan","last_name":"Ivanov","patronymic":"Ivanovich","birth_date":null,"user_address":"Country, City, Street, 15","phone":"+77777777777"}'
