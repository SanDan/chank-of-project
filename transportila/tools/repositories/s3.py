from boto.s3.key import Key
import boto
import time
from transportila import settings

__author__ = 'vol'

S3_BUCKET_URL = "https://s3.amazonaws.com/transportila2/"

class S3Repo:
    def __init__(self):
        self.bucket_name = settings.BUCKET
        self.conn = boto.connect_s3(settings.ACCESS_KEY_ID,
                                    settings.SECRET_ACCESS_KEY)
        self.S3_BUCKET_URL = "https://s3.amazonaws.com/transportila2/"
        self.bucket = self.conn.get_bucket(self.bucket_name)

    def is_key_exists(self, key):
        k = Key(self.bucket)
        k.key = key
        return k.exists()

    def send_file(self, data):
        s3_key = self.get_key(data['s3_subdir'] + data['filename'])
        s3_link = self.S3_BUCKET_URL + data['s3_subdir'] + data['filename']
        if 'content_as_string' in data:
            if 'content-type' in data:
                s3_key.set_metadata('content-type', data['content-type'])
            s3_key.set_contents_from_string(data['content_as_string'])
            s3_key.make_public()
        elif 'content_as_path' in data:
            s3_key.set_contents_from_filename(data['content_as_path'])
            s3_key.make_public()
        elif 'content_as_file' in data:
            if 'content-type' in data:
                s3_key.set_metadata('content-type', data['content-type'])
            s3_key.set_contents_from_file(data['content_as_file'])
            s3_key.make_public()
        return s3_link

    def get_key(self, path):
        s3_key = Key(self.bucket)
        s3_key.key = path
        return s3_key

    @staticmethod
    def get_subdir():
        return 'images/' + str(int(round(time.time() * 1000))) + '/'

    @staticmethod
    def get_filename(content_type):
        if content_type == 'image/jpeg':
            return str(int(time.time())) + '.jpg'

        elif content_type == 'image/png':
            return str(int(time.time())) + '.png'
        else:
            return str(int(time.time())) + '.unknown'
