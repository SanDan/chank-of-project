# -*- coding: utf-8 -*-
import datetime

from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
import pytz
from transportila.celery import app
from support.push import send_bet_decline_push

from orders.models import Bet

__author__ = 'sandan'


class CheckExpiredBet:
    def __init__(self):
        pass

    @staticmethod
    @app.task()
    def check(bet_id):
        try:
            bet = Bet.objects.get(pk=bet_id)
        except Bet.DoesNotExist:
            return u'Bet with id {0} does not created.'.format(bet_id)

        if bet.status == 1:
            bet.status = 8
            bet.save()
            send_bet_decline_push(
                bet,
                message=u"Срок подтверждения истек. Ваша ставка аннулированна",
                title=u"Ставка аннулированна"
            )
            order = bet.order_bettor.order
            if order.valid_until <= pytz.UTC.localize(datetime.datetime.utcnow()) and not Bet.objects.filter(
                    order_bettor__order=order, status=1).exists():
                order.set_auction_is_canceled_status()

            return u'Annulled bet with id {0}'.format(str(bet.id))

        return u'Bet have other status.'
        # subject = render_to_string(data['subject_template'])
        #
        # letter_data = data['params']
        #
        # text_content = render_to_string(data['template_text'], letter_data)
        # html_content = render_to_string(data['template_html'], letter_data)
        #
        # msg = EmailMultiAlternatives(subject, text_content, data['from_email'], data['emails_to'])
        # msg.attach_alternative(html_content, "text/html")
        # msg.send()
        # return u'Right send email to {0}'.format(str(data['emails_to']))
