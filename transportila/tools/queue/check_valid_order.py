# -*- coding: utf-8 -*-

from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from transportila.celery import app

from orders.models import Order, Bet

__author__ = 'sandan'


class CheckValidOrder:

    def __init__(self):
        pass

    @staticmethod
    @app.task()
    def check(order_uuid):
        try:
            order = Order.objects.get(pk=order_uuid)
        except Order.DoesNotExist:
            return u'Bet with id {0} does not exist.'.format(order_uuid)

        if order.status == 1:
            if Bet.objects.filter(order_bettor__order=order, status=1).exists():
                order.status = 6
            else:
                order.status = 2
            order.save()
            return u'Change order status to {0}'.format(str(order.status))

        return u'Order have other status.'
        # subject = render_to_string(data['subject_template'])
        #
        # letter_data = data['params']
        #
        # text_content = render_to_string(data['template_text'], letter_data)
        # html_content = render_to_string(data['template_html'], letter_data)
        #
        # msg = EmailMultiAlternatives(subject, text_content, data['from_email'], data['emails_to'])
        # msg.attach_alternative(html_content, "text/html")
        # msg.send()
        # return u'Right send email to {0}'.format(str(data['emails_to']))



