import time
from repositories.s3 import S3Repo

__author__ = 'vol'

from transportila.celery import app


class FileUploader:

    def __init__(self):
        pass

    @staticmethod
    @app.task()
    def upload(msg):
        data = msg['data']
        file_ = S3Repo().send_file(data)
        return file_
