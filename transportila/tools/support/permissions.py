from rest_framework.permissions import BasePermission

__author__ = 'sandan'

class CanAddOrder(BasePermission):
    """
    Allows access for add order only to customers and dispatchers.
    """

    def has_permission(self, request, view):
        return request.user and request.user.has_perms(['orders.add_order'], )
