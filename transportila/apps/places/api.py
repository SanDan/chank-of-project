# coding=utf-8
__author__ = 'slyfest'

from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet
from oauth2_provider.ext.rest_framework import TokenHasReadWriteScope
from rest_framework import permissions
from rest_framework.pagination import PageNumberPagination
from .serializers import *
from .models import *


class CountriesViewSet(ModelViewSet):
    model = Country
    serializer_class = CountrySerializer
    permission_classes = ()
    queryset = Country.objects.filter(in_profile=True).order_by('title')

    def list(self, request, *args, **kwargs):
        return Response(CountrySerializer(self.get_queryset(), many=True).data)


class TypeServiceViewSet(ModelViewSet):
    model = TypeService
    serializer_class = TypeServiceSerializer
    queryset = TypeService.objects.all()
    permission_classes = (permissions.IsAuthenticated, TokenHasReadWriteScope)

    def list(self, request, *args, **kwargs):
        return Response(TypeServiceSerializer(self.get_queryset(), many=True).data)


class TypePlaceViewSet(ModelViewSet):
    model = TypePlace
    serializer_class = TypePlaceSerializer
    queryset = TypePlace.objects.all()
    permission_classes = (permissions.IsAuthenticated, TokenHasReadWriteScope)

    def list(self, request, *args, **kwargs):
        return Response(TypePlaceSerializer(self.get_queryset(), many=True).data)


class CustomPagination(PageNumberPagination):
    page_size = 30


class PlaceViewSet(ModelViewSet):
    model = Place
    serializer_class = PlaceShortSerializer
    queryset = Place.objects.all()
    permission_classes = (permissions.IsAuthenticated, TokenHasReadWriteScope)
    paginator = CustomPagination()

    def list(self, request, *args, **kwargs):
        if 'q' in request.query_params:
            places_types = []
            if 'place_type_id' in request.query_params:
                places_types = request.query_params['place_type_id'].split(',')

            place_list = self.model.place_search(query=request.query_params['q'], places_types=places_types)
            result_page = self.paginator.paginate_queryset(place_list, request)

            serializer = self.serializer_class(result_page, many=True)

            response = self.paginator.get_paginated_response(serializer.data)
        else:
            request_serializer = RequestPlacesListSerializer(data=request.query_params)
            request_serializer.is_valid(raise_exception=True)

            places_types = []
            if 'place_type_id' in request.query_params and request.query_params['place_type_id'] != '':
                places_types = request.query_params['place_type_id'].split(',')

            place_list = self.model.get_in_square(
                places_types,
                request.query_params['south_west_lat'],
                request.query_params['south_west_lng'],
                request.query_params['north_east_lat'],
                request.query_params['north_east_lng']
            )

            result_page = self.paginator.paginate_queryset(place_list, request)

            serializer = self.serializer_class(result_page, many=True, context={'request': request})

            response = self.paginator.get_paginated_response(serializer.data)

        return response

    def retrieve(self, request, *args, **kwargs):
        return Response(PlaceDetailSerializer(self.get_object()).data)
