# coding=utf-8
import json
from django.core.management.base import BaseCommand
import goslate
from slugify import slugify
from places.models import Country, City, Address, AddressCoord
import geocoder


class Command(BaseCommand):
    args = '<>'
    help = 'Prepares the database'

    def handle(self, *args, **options):

        self.stdout.write("Preparing db data for address\n")

        # add slug to country
        # countries = list(Country.objects.all())
        # for item in countries:
        #     if item.title_en:
        #         item.slug = slugify(item.title_en, to_lower=True)
        #         item.save()

        # cities = list(City.objects.select_related('country').all())
        # for item in cities:
        #     if item.title_en and item.latitude and item.longitude:
        #         item.slug = slugify(u'{0} {1} {2}'.format(item.title_en, int(item.latitude), int(item.longitude)), to_lower=True)
        #         item.country_slug = item.country.slug
        #         item.save()
        #         print u'added {0}'.format(item.slug)
        #     else:
        #         print u'not added - {0}'.format(item.title_en)



        # none addresses 1
        # not used addresses 127

        # addresses = list(Address.objects.prefetch_related(
        #     'address_places', 'address_basic_info', 'address_companies').all())
        # none_address = 0
        # not_used = 0
        # for item in addresses:
        #     if item.title == None:
        #         none_address += 1
        #     if not (item.address_places.exists() or item.address_basic_info.exists() or item.address_companies.exists()):
        #         not_used += 1
        # print u'none addresses {0}'.format(none_address)
        # print u'not used addresses {0}'.format(not_used)

        #############

        # clear duplicate countries

        # countries = list(Country.objects.order_by('slug').distinct('slug'))
        #
        # for item in countries:
        #     print(u'item ---------------------- {0}'.format(item.slug))
        #     duplicates = list(Country.objects.filter(slug=item.slug))
        #     if len(duplicates) > 1:
        #         for d in duplicates:
        #             if d != item:
        #                 print(u'remove - {0}'.format(d.slug))
        #                 d.delete()

        # delete None country
        # country = Country.objects.get(slug=None)
        # country.delete()

        # add city_slug to addresses
        # addresses = list(Address.objects.select_related('city').all())
        # for item in addresses:
        #     if item.city.slug:
        #         item.city_slug = item.city.slug
        #         item.save()
        #         print(u'{0}'.format(item.city_slug))

        # remove cities None slug and None Country
        # cities_none_slug = list(City.objects.filter(slug=None))
        # for item in cities_none_slug:
        #     print(u'remove - {0}'.format(item.title))
        #     item.delete()
        #
        # cities_none_country = list(City.objects.filter(country_slug=None))
        # for item in cities_none_country:
        #     print(u'remove - {0}'.format(item.title))
        #     item.delete()

        # remove duplicated cities
        # cities = City.objects.order_by('slug').distinct('slug')
        # for item in cities:
        #     duplicates = list(City.objects.filter(slug=item.slug))
        #     if len(duplicates) > 1:
        #         for d in duplicates:
        #             if d != item:
        #                 print(u'remove {0}'.format(d.title))
        #                 d.delete()

        # bind cities to countries
        # countries = dict()
        # for item in list(Country.objects.all()):
        #     countries[item.slug] = item
        #
        # cities = list(City.objects.all())
        # for city in cities:
        #     city.country = countries[city.country_slug]
        #     city.save()

        # create address coord and bind with address
        # addresses = list(Address.objects.all())
        # for item in addresses:
        #     address_coord, is_created = AddressCoord.objects.get_or_create(
        #         latitude=item.latitude,
        #         longitude=item.longitude,
        #         defaults=dict(
        #             city_slug=item.city_slug
        #         )
        #     )
        #     print u'{0}'.format(item.title_en)
        #     item.address_coord = address_coord
        #     item.save()

        # change latitude and longitude in slug
        # cities = list(City.objects.all())
        # for item in cities:
        #     if item.latitude and item.longitude and item.title_en:
        #         item.new_slug = slugify(u'{0} {1} {2}'.format(item.title_en, int(item.latitude), int(item.longitude)), to_lower=True)
        #         item.save()
        #         print u'added {0}'.format(item.slug)
        #     else:
        #         print u'remove {0}'.format(item.slug)
        #         item.delete()


        countries_codes = {"BD": "Bangladesh", "BE": "Belgium", "BF": "Burkina Faso", "BG": "Bulgaria",
             "BA": "Bosnia and Herzegovina", "BB": "Barbados", "WF": "Wallis and Futuna", "BL": "Saint Barthelemy",
             "BM": "Bermuda", "BN": "Brunei", "BO": "Bolivia", "BH": "Bahrain", "BI": "Burundi", "BJ": "Benin",
             "BT": "Bhutan", "JM": "Jamaica", "BV": "Bouvet Island", "BW": "Botswana", "WS": "Samoa",
             "BQ": "Bonaire, Saint Eustatius and Saba ", "BR": "Brazil", "BS": "Bahamas", "JE": "Jersey",
             "BY": "Belarus", "BZ": "Belize", "RU": "Russia", "RW": "Rwanda", "RS": "Serbia", "TL": "East Timor",
             "RE": "Reunion", "TM": "Turkmenistan", "TJ": "Tajikistan", "RO": "Romania", "TK": "Tokelau",
             "GW": "Guinea-Bissau", "GU": "Guam", "GT": "Guatemala",
             "GS": "South Georgia and the South Sandwich Islands", "GR": "Greece", "GQ": "Equatorial Guinea",
             "GP": "Guadeloupe", "JP": "Japan", "GY": "Guyana", "GG": "Guernsey", "GF": "French Guiana",
             "GE": "Georgia", "GD": "Grenada", "GB": "United Kingdom", "GA": "Gabon", "SV": "El Salvador",
             "GN": "Guinea", "GM": "Gambia", "GL": "Greenland", "GI": "Gibraltar", "GH": "Ghana", "OM": "Oman",
             "TN": "Tunisia", "JO": "Jordan", "HR": "Croatia", "HT": "Haiti", "HU": "Hungary", "HK": "Hong Kong",
             "HN": "Honduras", "HM": "Heard Island and McDonald Islands", "VE": "Venezuela", "PR": "Puerto Rico",
             "PS": "Palestinian Territory", "PW": "Palau", "PT": "Portugal", "SJ": "Svalbard and Jan Mayen",
             "PY": "Paraguay", "IQ": "Iraq", "PA": "Panama", "PF": "French Polynesia", "PG": "Papua New Guinea",
             "PE": "Peru", "PK": "Pakistan", "PH": "Philippines", "PN": "Pitcairn", "PL": "Poland",
             "PM": "Saint Pierre and Miquelon", "ZM": "Zambia", "EH": "Western Sahara", "EE": "Estonia", "EG": "Egypt",
             "ZA": "South Africa", "EC": "Ecuador", "IT": "Italy", "VN": "Vietnam", "SB": "Solomon Islands",
             "ET": "Ethiopia", "SO": "Somalia", "ZW": "Zimbabwe", "SA": "Saudi Arabia", "ES": "Spain", "ER": "Eritrea",
             "ME": "Montenegro", "MD": "Moldova", "MG": "Madagascar", "MF": "Saint Martin", "MA": "Morocco",
             "MC": "Monaco", "UZ": "Uzbekistan", "MM": "Myanmar", "ML": "Mali", "MO": "Macao", "MN": "Mongolia",
             "MH": "Marshall Islands", "MK": "Macedonia", "MU": "Mauritius", "MT": "Malta", "MW": "Malawi",
             "MV": "Maldives", "MQ": "Martinique", "MP": "Northern Mariana Islands", "MS": "Montserrat",
             "MR": "Mauritania", "IM": "Isle of Man", "UG": "Uganda", "TZ": "Tanzania", "MY": "Malaysia",
             "MX": "Mexico", "IL": "Israel", "FR": "France", "IO": "British Indian Ocean Territory",
             "SH": "Saint Helena", "FI": "Finland", "FJ": "Fiji", "FK": "Falkland Islands", "FM": "Micronesia",
             "FO": "Faroe Islands", "NI": "Nicaragua", "NL": "Netherlands", "NO": "Norway", "NA": "Namibia",
             "VU": "Vanuatu", "NC": "New Caledonia", "NE": "Niger", "NF": "Norfolk Island", "NG": "Nigeria",
             "NZ": "New Zealand", "NP": "Nepal", "NR": "Nauru", "NU": "Niue", "CK": "Cook Islands", "XK": "Kosovo",
             "CI": "Ivory Coast", "CH": "Switzerland", "CO": "Colombia", "CN": "China", "CM": "Cameroon", "CL": "Chile",
             "CC": "Cocos Islands", "CA": "Canada", "CG": "Republic of the Congo", "CF": "Central African Republic",
             "CD": "Democratic Republic of the Congo", "CZ": "Czech Republic", "CY": "Cyprus", "CX": "Christmas Island",
             "CR": "Costa Rica", "CW": "Curacao", "CV": "Cape Verde", "CU": "Cuba", "SZ": "Swaziland", "SY": "Syria",
             "SX": "Sint Maarten", "KG": "Kyrgyzstan", "KE": "Kenya", "SS": "South Sudan", "SR": "Suriname",
             "KI": "Kiribati", "KH": "Cambodia", "KN": "Saint Kitts and Nevis", "KM": "Comoros",
             "ST": "Sao Tome and Principe", "SK": "Slovakia", "KR": "South Korea", "SI": "Slovenia",
             "KP": "North Korea", "KW": "Kuwait", "SN": "Senegal", "SM": "San Marino", "SL": "Sierra Leone",
             "SC": "Seychelles", "KZ": "Kazakhstan", "KY": "Cayman Islands", "SG": "Singapore", "SE": "Sweden",
             "SD": "Sudan", "DO": "Dominican Republic", "DM": "Dominica", "DJ": "Djibouti", "DK": "Denmark",
             "VG": "British Virgin Islands", "DE": "Germany", "YE": "Yemen", "DZ": "Algeria", "US": "United States",
             "UY": "Uruguay", "YT": "Mayotte", "UM": "United States Minor Outlying Islands", "LB": "Lebanon",
             "LC": "Saint Lucia", "LA": "Laos", "TV": "Tuvalu", "TW": "Taiwan", "TT": "Trinidad and Tobago",
             "TR": "Turkey", "LK": "Sri Lanka", "LI": "Liechtenstein", "LV": "Latvia", "TO": "Tonga", "LT": "Lithuania",
             "LU": "Luxembourg", "LR": "Liberia", "LS": "Lesotho", "TH": "Thailand",
             "TF": "French Southern Territories", "TG": "Togo", "TD": "Chad", "TC": "Turks and Caicos Islands",
             "LY": "Libya", "VA": "Vatican", "VC": "Saint Vincent and the Grenadines", "AE": "United Arab Emirates",
             "AD": "Andorra", "AG": "Antigua and Barbuda", "AF": "Afghanistan", "AI": "Anguilla",
             "VI": "U.S. Virgin Islands", "IS": "Iceland", "IR": "Iran", "AM": "Armenia", "AL": "Albania",
             "AO": "Angola", "AQ": "Antarctica", "AS": "American Samoa", "AR": "Argentina", "AU": "Australia",
             "AT": "Austria", "AW": "Aruba", "IN": "India", "AX": "Aland Islands", "AZ": "Azerbaijan", "IE": "Ireland",
             "ID": "Indonesia", "UA": "Ukraine", "QA": "Qatar", "MZ": "Mozambique"}
        # bind address coord with city
        # addresses_coord = list(AddressCoord.objects.all())
        #
        # for item in addresses_coord:
        #     if not item.city:
        #         try:
        #             city = City.objects.get(slug=item.city_slug)
        #         except City.DoesNotExist:
        #             city = None
        #
        #         # create city or/and country
        #         if not city and -170 <= item.latitude <= 170 and -170 <= item.longitude <= 170:
        #             # google geocode
        #             # g = geocoder.google([item.latitude, item.longitude], method='reverse')
        #             # gs = goslate.Goslate()
        #             # # print item.latitude, item.longitude
        #             # # print dir(g)
        #
        #             # country_title = g.country_long
        #             # city_title = g.city
        #
        #             g = geocoder.mapquest([item.latitude, item.longitude], method='reverse')
        #             gs = goslate.Goslate()
        #             # print item.latitude, item.longitude
        #             # print dir(g)
        #
        #             # print dir(g)
        #             print g.country
        #             country_title = countries_codes[g.country] if g.country in countries_codes else None
        #             city_title = g.city
        #
        #             print u'{0}, {1}'.format(country_title, city_title)
        #             print u'++++++++++++++++++++++++++++++++'
        #             if not (country_title and city_title):
        #                 item.city_slug = None
        #                 item.save()
        #                 continue
        #
        #
        #
        #             try:
        #                 country_ru_translation = gs.translate(country_title, 'ru')
        #             except:
        #                 country_ru_translation = None
        #
        #             try:
        #                 city_ru_translation = gs.translate(city_title, 'ru')
        #             except:
        #                 city_ru_translation = None
        #
        #             latitude = g.lat
        #             longitude = g.lng
        #
        #             # print country_title, city_title
        #
        #             c = geocoder.mapquest(country_title)
        #             country_latitude = c.lat
        #             country_longitude = c.lng
        #
        #             f = geocoder.mapquest(city_title)
        #             city_latitude = f.lat
        #             city_longitude = f.lng
        #
        #             if not (city_longitude and city_latitude):
        #                 item.city_slug = None
        #                 item.save()
        #                 continue
        #             print item.latitude, item.longitude
        #             print g.country, g.city
        #             print u'-------------------------'
        #
        #             country, is_created = Country.objects.get_or_create(
        #                 slug=slugify(country_title, to_lower=True),
        #                 defaults=dict(
        #                     title=country_title,
        #                     latitude=country_latitude,
        #                     longitude=country_longitude,
        #                     title_en=country_title,
        #                     title_ru=country_ru_translation
        #                 )
        #
        #             )
        #
        #             city, is_created = City.objects.get_or_create(
        #                 slug=slugify('{0} {1} {2}'.format(str(city_title), int(city_latitude), int(city_longitude)),
        #                              to_lower=True),
        #                 defaults=dict(
        #                     country=country,
        #                     title=city_title,
        #                     latitude=city_latitude,
        #                     longitude=city_longitude,
        #                     title_en=city_title,
        #                     title_ru=city_ru_translation
        #                 )
        #             )
        #         item.city = city
        #         item.save()

        # get count of address coord without city
        # print AddressCoord.objects.filter(city=None).count()

        # clean countries
        # cities = list(City.objects.all())
        # for item in cities:
        #     if item.country.title_en.upper() in countries_codes:
        #         try:
        #             country = Country.objects.get(slug=slugify(countries_codes[item.country.title_en.upper()], to_lower=True))
        #         except Country.DoesNotExist:
        #             continue
        #         print u'update city - {0}, country - {1}'.format(item.slug, country.slug)
        #         item.country = country
        #         item.save()

        # countries = Country.objects.all()
        # for item in countries:
        #     if item.title_en.upper() in countries_codes:
        #         print u'removed - {0}'.format(item.title_en)
        #         item.delete()

        # bind address with address coord
        # addresses = list(Address.objects.all())
        # for item in addresses:
        #     try:
        #         address_coord = AddressCoord.objects.get(latitude=item.latitude, longitude=item.longitude)
        #     except AddressCoord.DoesNotExist:
        #         continue
        #     print address_coord.latitude, address_coord.longitude
        #     item.address_coord = address_coord
        #     item.save()

        # destroy bad addresses
        # for item in list(Address.objects.filter(address_coord=None)):
        #     for sub_item in list(item.address_places.all()):
        #         sub_item.delete()
        #
        #     item.delete()

        # print Address.objects.filter(address_coord=None).count()

        # add iso slug to countries
        # countries = list(Country.objects.all())
        # for index, item in enumerate(countries):
        #     if item.latitude and item.longitude:
        #         g = geocoder.mapquest([item.latitude, item.longitude], method="reverse")
        #         item.iso_code = slugify(g.country, to_lower=True)
        #         item.save()
        #         print g.country
        # country = Country.objects.get(slug='antarctica')
        # for item in list(City.objects.filter(country=country)):
        #     print item.title

        # add iso country slug to city
        # cities = list(City.objects.all())
        # for index, item in enumerate(cities):
        #     if item.latitude and item.longitude and not item.country_iso_code:
        #         g = geocoder.mapquest([item.latitude, item.longitude], method="reverse")
        #         if g.country:
        #             slug = slugify(g.country, to_lower=True)
        #             item.country_iso_code = slug
        #             item.save()
        #             print slug
        #         else:
        #             print u'not country title'
        #         del g


        # remove duplicated addresses
        # addresses = list(Address.objects.all())
        # sorted_addresses = list()
        #
        # for item in addresses:
        #     is_saw = False
        #     for index, s in enumerate(sorted_addresses):
        #         if s['obj'].title == item.title and s['obj'].address_coord.id == item.address_coord.id:
        #             is_saw = True
        #             break
        #     if is_saw:
        #         if sorted_addresses[index]['obj'].id != item.id:
        #             sorted_addresses[index]['duplicates'].append(item)
        #             print u'find duplication {0}'.format(item.title)
        #     else:
        #         sorted_addresses.append(
        #             dict(obj=item, duplicates=list())
        #         )
                # print u'added {0}'.format(item.title)

        # cities = City.objects.filter(country_iso_code='mx')
        # for item in cities:
        #     print item.country.slug

        #
        # sorted_countries = dict()
        # for item in list(Country.objects.all()):
        #     if item.iso_code in sorted_countries:
        #         sorted_countries[item.iso_code]['duplicates'].append(item)
        #     else:
        #         sorted_countries[item.iso_code] = dict(
        #             obj=item,
        #             duplicates=list()
        #         )

        # bind countries
        # countries = dict()
        # for item in list(Country.objects.all()):
        #     countries[item.slug] = item
        #
        # for item in list(City.objects.filter(country=None)):
        #     if item.country_iso_code in countries:
        #         item.country = countries[item.country_iso_code]
        #         item.save()
        #         print u'update {0}'.format(item.title_en)

        # check cities without countries
        # print City.objects.filter(country=None).count()

        #remove volve-58-1 address coord
        # address_coord = AddressCoord.objects.filter(city__slug='volve-58-1').first()
        # address_coord.city = None
        # address_coord.is_correct = False
        # address_coord.save()

        # remove duplicates in address
        addresses = dict()
        for item in list(Address.objects.all()):
            if u'{0}-{1}'.format(item.title_en, item.address_coord.id) in addresses:
                addresses[u'{0}-{1}'.format(item.title_en, item.address_coord.id)]['duplicates'].append(item)
            else:
                addresses[u'{0}-{1}'.format(item.title_en, item.address_coord.id)] = dict(
                    obj=item,
                    duplicates=list()
                )
        for item in addresses:
            for subitem in addresses[item]['duplicates']:
                print u'remove {0}'.format(subitem.title_en)
                subitem.delete()

        # address = Address.make(dict(latitude=58.4536850, longitude=34.8892000), 'coord')
        # print address.title
        self.stdout.write("Successfully complete\n")
