# coding=utf-8
__author__ = 'Slyfest'

from django.core.management.base import BaseCommand
from push_notifications.models import GCMDevice, APNSDevice
from accounts.models import Device

import json


android_test = Device.objects.get(user_id=8)

data = dict(
    push_notification_type=2,
    info=dict(
        message_push_type=2,
        msg_body=dict(
            order_id=1,
            title="response",
            msg="Azazaza push"
        )
    )
)

data = json.dumps(data)


apns_token = "1427e18a a563e5a5 17ed17a2 070b65bb 3a7ee8bf 7e3eab5c 0027c816 658481f8"
apns_clean = apns_token.replace(" ", "")


class Command(BaseCommand):
    args = '<>'
    help = 'Push notifications test'

    def handle(self, *args, **options):

        # device = GCMDevice.objects.get(registration_id=android_test.key)
        #
        # device.send_message(None, extra=dict(data=data))

        ios_device = APNSDevice.objects.get(registration_id=apns_clean)
        ios_device.send_message(None, extra=dict(data=data))