__author__ = 'Slyfest'

import json

from django.core.management.base import BaseCommand
from django.db.models import Q
import os
from places.models import Address


class Command(BaseCommand):
    args = '<>'
    help = 'Parses marinetraffic.com'

    def handle(self, *args, **options):

        duplicate_list = Address.objects.filter(latitude=-33.933028, longitude=151.1885905)

        for entry in duplicate_list:
            print entry