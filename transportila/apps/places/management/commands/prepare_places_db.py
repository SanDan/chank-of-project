__author__ = 'slyfest'

from django.core.management.base import BaseCommand

from places.models import *


def add_type_place(title):
    type_place, is_created = TypePlace.objects.get_or_create(
        title=title,
    )
    return type_place


def add_type_service(title, type_place):
    type_service, is_created = TypeService.objects.get_or_create(
        title=title,
        type_place=type_place
    )
    return type_service


def add_service(title, type_service):
    Service.objects.get_or_create(
        title=title,
        type_service=type_service,
    )


def prepare_place(type_service_title, type_place_title, service_title):
    type_place = add_type_place(type_place_title)
    type_service = add_type_service(type_service_title, type_place)
    add_service(service_title, type_service)


class Command(BaseCommand):
    args = '<>'
    help = 'Prepares the database'

    def handle(self, *args, **options):

        self.stdout.write("Preparing db data for 'places' app\n")
        prepare_place('cargo', 'airport', '1 kg weight cargo')
        prepare_place('storage', 'warehouse', 'medium-storage service')
        prepare_place('cargo', 'port', '1 kg weight cargo')

        self.stdout.write("Successfully complete\n")
