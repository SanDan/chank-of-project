# coding=utf-8
__author__ = 'Slyfest'

import scrapy
from scrapy.selector import Selector
from scrapy.http import Request
from ..items import AirportsParseItem


class AirportSpider(scrapy.Spider):
    name = "spidy"
    allowed_domains = ["airport.airlines-inform.ru"]
    start_urls = ["http://airport.airlines-inform.ru/countries"]

    def parse(self, response):

        sel = Selector(response).xpath('//div[@class="catalog-section-list"]')

        for data in sel.xpath('ul/li/a'):
            hrefs = data.xpath('@href').extract()

            for href in hrefs:
                yield Request(href, callback=self.parse_airport_link)

    def parse_airport_link(self, response):

        sel = Selector(response).xpath('//a[@class="airlines"]')

        for href in sel.xpath('@href').extract():
            yield Request(href, callback=self.parse_airport_details)

    def parse_airport_details(self, response):
        item = AirportsParseItem()

        title = Selector(response).xpath('//div[@class="catalog-element"]/h1/text()').extract()
        sel = Selector(response).xpath('//div[@class="detail_airport"]')
        stars = Selector(response).xpath('//img[@src="/images/star.gif"]').extract()
        description = sel.xpath('b/following-sibling::text()').extract()
        website_list = sel.xpath('a[@target="new"]/@href').extract()
        if len(website_list) > 0:
            website = website_list[0]
        else:
            website = ''
        iata = ''
        for entry in description:
            if len(entry.strip()) == 3:
                iata = entry
        rating = len(stars) * 20
        item['title'] = title[0].split('(', 1)[1][:-1]
        item['iata'] = iata
        item['website'] = website
        item['rating'] = rating

        yield item


