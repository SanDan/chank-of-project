__author__ = 'slyfest'

from django.conf.urls import url, include
from rest_framework import routers

from .api import *


v1_router = routers.SimpleRouter()
v1_router.register(r'places/countries', CountriesViewSet)
v1_router.register(r'type_services', TypeServiceViewSet)
v1_router.register(r'type_places', TypePlaceViewSet)
v1_router.register(r'places', PlaceViewSet)


v1_urlpatterns = [
    url(r'', include(v1_router.urls))
]


urlpatterns = [
    url(r'^api/v1/', include(v1_urlpatterns)),
]

