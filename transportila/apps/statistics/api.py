__author__ = 'Slyfest'

from datetime import datetime, timedelta

from rest_framework import permissions
from oauth2_provider.ext.rest_framework import TokenHasReadWriteScope
from rest_framework.response import Response
from rest_framework import status
from rest_framework.viewsets import ModelViewSet
from rest_framework.pagination import PageNumberPagination
from statistics.serializers import RatingShortSerializer, RatingRequestSerializer, RatingCreateSerializer, \
    RatingDetailSerializer
from auxiliaries.models import Comment, TransporterReview
from django.db.models import Q


class CustomPagination(PageNumberPagination):
    page_size = 10


class RatingViewSet(ModelViewSet):
    model = TransporterReview
    serializer_class = RatingShortSerializer
    queryset = TransporterReview.objects.all()
    permission_classes = (permissions.IsAuthenticated, TokenHasReadWriteScope)
    paginator = CustomPagination()

    def create(self, request, *args, **kwargs):
        serializer = RatingCreateSerializer(data=self.request.data, context=dict(request=request))
        serializer.is_valid()
        comment = serializer.save()

        return Response(self.serializer_class(comment, context=dict(request=request)).data, status=status.HTTP_201_CREATED)

    def list(self, request, *args, **kwargs):
        if 'list' in request.query_params:
            request_serlializer = RatingRequestSerializer(data=request.query_params)
            request_serlializer.is_valid()

            comment_list = self.get_queryset().filter(user=request_serlializer.validated_data.get('transporter'))
            result_page = self.paginator.paginate_queryset(comment_list, request)
            serializer = RatingDetailSerializer(result_page, many=True, context=dict(request=request))

            return self.paginator.get_paginated_response(serializer.data)

        request_serlializer = RatingRequestSerializer(data=request.query_params)
        request_serlializer.is_valid()

        count_good = TransporterReview.objects.filter(
            Q(user=request_serlializer.validated_data.get('transporter')),
            Q(mark=1),
            Q(created_at__gte=datetime.now() - timedelta(days=365))
        ).count()

        count_bad = TransporterReview.objects.filter(
            Q(user=request_serlializer.validated_data.get('transporter')),
            Q(mark=0),
            Q(created_at__gte=datetime.now() - timedelta(days=365))
        ).count()

        try:
            ratio = count_good * 100 / (count_good + count_bad)
        except ZeroDivisionError:
            ratio = 0

        statistics = dict(
            transporter_id=request.query_params['transporter'],
            count_good=count_good,
            count_bad=count_bad,
            ratio=ratio
        )

        return Response(statistics)
