__author__ = 'Slyfest'

import datetime

from django.db.models import Q
from rest_framework.serializers import SerializerMethodField, IntegerField, CharField, UUIDField
from support.serializers import CustomSerializer, CustomModelSerializer
from support.exceptions import ApiStandartException
from auxiliaries.models import TransporterReview
from accounts.models import CustomUser
from orders.models import Order


class RatingRequestSerializer(CustomSerializer):
    transporter = IntegerField()

    def validate_transporter(self, transporter):
        transporter_obj = CustomUser.objects.filter(pk=transporter).first()
        if transporter_obj:
            return transporter_obj
        else:
            raise ApiStandartException(dict(error_code='400100'))


class RatingCreateSerializer(CustomSerializer):
    order_id = UUIDField()
    text = CharField(required=False)
    mark = IntegerField(min_value=0, max_value=1)
    user = IntegerField()

    def validate_order_id(self, order_id):
        order = Order.objects.filter(
            Q(uuid=order_id),
            Q(status=5),
        ).first()
        return order.uuid

    def validate_text(self, text):
        return text

    def validate_mark(self, mark):
        return mark

    def validate_user(self, user):
        return CustomUser.objects.get(pk=user)

    class Meta:
        model = TransporterReview
        fields = ('text', 'rating')

    def create(self, validated_data):
        if TransporterReview.objects.filter(
                Q(commentator=self.context['request'].user),
                Q(order_id=validated_data.get('order_id'))
        ):
            raise ApiStandartException(dict(error_code='400101'))
        else:
            review = TransporterReview.objects.create(
                text=self.validated_data.get('text'),
                order_id=self.validated_data.get('order_id'),
                user=self.validated_data.get('user'),
                mark=self.validated_data.get('mark'),
                created_at=datetime.datetime.now(),
                commentator=validated_data.get('commentator')
            )
            return review


class RatingShortSerializer(CustomModelSerializer):
    class Meta:
        model = TransporterReview
        fields = ('order_id', 'text', 'mark', 'commentator', 'created_at', 'user')


class RatingDetailSerializer(CustomModelSerializer):
    commentator = SerializerMethodField()
    message = SerializerMethodField()
    mark = SerializerMethodField()

    def get_mark(self, transporter_review):

        if transporter_review.mark == 0:
            mark = 'Bad'
        elif transporter_review.mark == 1:
            mark = 'Good'
        return dict(
            slug=mark,
            value=transporter_review.mark
        )

    def get_commentator(self, transporter_review):
        commentator = transporter_review.commentator
        if self.context['request'].user == transporter_review.user:
            return dict(
                nickname=commentator.nickname,
                id=commentator.id
            )
        else:
            return dict(
                nickname=str(commentator.nickname)[0] + '***' + str(commentator.nickname)[-2:],
                id=commentator.id
            )

    def get_message(self, transporter_review):
        return transporter_review.text

    class Meta:
        model = TransporterReview
        fields = ('message', 'commentator', 'mark', 'created_at', 'order_id')
