__author__ = 'Slyfest'

from django.conf.urls import url, include
from rest_framework import routers

from statistics.api import RatingViewSet


v1_router = routers.SimpleRouter()
v1_router.register(r'statistics', RatingViewSet)

v1_urlpatterns = [
    url(r'', include(v1_router.urls))
]

urlpatterns = [
    url(r'^api/v1/', include(v1_urlpatterns)),
]