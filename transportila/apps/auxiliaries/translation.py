from modeltranslation.translator import register, TranslationOptions
from .models import Characteristic, CharacteristicUnit, Color

__author__ = 'sandan'


@register(Characteristic)
class CharacteristicTranslationOptions(TranslationOptions):
    fields = ('title',)


@register(CharacteristicUnit)
class CharacteristicUnitTranslationOptions(TranslationOptions):
    fields = ('title',)


@register(Color)
class ColorTranslationOptions(TranslationOptions):
    fields = ('title',)
