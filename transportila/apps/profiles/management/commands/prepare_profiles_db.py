from django.core.management.base import BaseCommand
import profiles


class Command(BaseCommand):
    args = '<>'
    help = 'Prepares the database'

    def handle(self, *args, **options):

        self.stdout.write("Preparing db data for 'profiles' app\n")
        profiles.add()

        self.stdout.write("Successfully complete\n")
