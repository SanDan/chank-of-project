# coding=utf-8
try:
    from django.contrib.auth import get_user_model
    User = get_user_model()
except ImportError:
    from django.contrib.auth.models import User
from rest_framework.serializers import SerializerMethodField, CharField, EmailField, IntegerField, SlugRelatedField

from support.helpers import Versioner, FieldFullness
from support.serializers import CustomModelSerializer, CustomSerializer
from .models import BasicInfo, BillingInfo, DocumentInfo
from company.models import CompanyInfo
from auxiliaries.models import Attachment
from places.models import Address, Country
from places.serializers import CountryRelatedField

__author__ = 'vol'


class BasicInfoSerializer(CustomModelSerializer):
    class Meta:
        model = BasicInfo
        fields = ('id', 'first_name', 'last_name', 'patronymic', 'birth_date', 'user_address', 'phone',
                  'created_at', 'version')

    def save(self, **kwargs):
        self.validated_data['user'] = self.context['request'].user
        if self.validated_data['user_address']:
            self.validated_data['address'] = Address.make_from_string(self.validated_data.get('user_address'))
        validated_data_versioned = Versioner.get_version('BasicInfo', self.validated_data)
        return BasicInfo.objects.create(**validated_data_versioned)

    def update(self, instance, validated_data):
        pass


class BillingInfoSerializer(CustomModelSerializer):
    country = CountryRelatedField(queryset=Country.objects.all(), slug_field='slug')

    class Meta:
        model = BillingInfo
        fields = ('id', 'name',
                  'country', 'address', 'email', 'phone', 'zip', 'version', 'created_at', 'confirmed')

    def save(self, **kwargs):
        self.validated_data['user'] = self.context['request'].user
        validated_data_versioned = Versioner.get_version('BillingInfo', self.validated_data)
        return BillingInfo.objects.create(**validated_data_versioned)

    def update(self, instance, validated_data):
        pass


class DocumentInfoSerializer(CustomModelSerializer):
    class Meta:
        model = DocumentInfo
        fields = ('id', 'passport_id', 'iin', 'license_id', 'created_at', 'version')

    def save(self, **kwargs):
        self.validated_data['user'] = self.context['request'].user
        validated_data_versioned = Versioner.get_version('DocumentInfo', self.validated_data)
        return DocumentInfo.objects.create(**validated_data_versioned)

    def update(self, instance, validated_data):
        pass


class ProfileSerializer(CustomModelSerializer):
    user_group = SerializerMethodField()
    billing = SerializerMethodField()
    basic = SerializerMethodField()
    document = SerializerMethodField()
    company = SerializerMethodField()
    avatar = SerializerMethodField()

    def get_user_group(self, user):
        return user.get_user_group()

    def get_billing(self, user):
        return FieldFullness(BillingInfo.get_by_user(user)).get_fullness()

    def get_basic(self, user):
        return FieldFullness(BasicInfo.get_by_user(user)).get_fullness()

    def get_document(self, user):
        return FieldFullness(DocumentInfo.get_by_user(user)).get_fullness()

    def get_company(self, user):
        return FieldFullness(CompanyInfo.get_by_user(user)).get_fullness()

    def get_avatar(self, user):
        avatar = user.get_avatar()
        if avatar:
            return dict(url=avatar.url, id=avatar.id)
        return None

    class Meta:
        model = User
        fields = ('id', 'nickname', 'email', 'user_group', 'billing', 'basic', 'document', 'company', 'avatar')


class UpdateProfileSerializer(CustomSerializer):
    avatar = IntegerField()
    nickname = CharField(max_length=128)
    # email = EmailField(max_length=256)

    def validate_avatar(self, avatar_id):
        return Attachment.get_by_id(avatar_id)

    def validate_nickname(self, nickname):
        self.context['request'].user.check_nickname(nickname)
        return nickname

    def update(self, instance, validated_data):
        instance.nickname = validated_data.get('nickname')
        avatar = instance.get_avatar()
        if avatar is not None and avatar.id != validated_data.get('avatar').id:
            avatar.delete()
        validated_data.get('avatar').set_content_type(instance)
        instance.save()
        return instance
