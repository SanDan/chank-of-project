# coding=utf-8
from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _

class VehiclesConfig(AppConfig):
    name = 'vehicles'
    verbose_name = _(u'Vehicles')