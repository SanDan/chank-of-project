# coding=utf-8
from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.db.models import Model, CharField, ForeignKey, BooleanField, ManyToManyField, FloatField, TextField, \
    DateTimeField, DateField, PositiveIntegerField, Q
from django.utils.translation import ugettext_lazy as _

try:
    User = settings.AUTH_USER_MODEL
except ImportError:
    from django.contrib.auth.models import User

from support.exceptions import ApiStandartException
from auxiliaries.models import Characteristic, Attachment, Color


class VehicleBrand(Model):
    title = CharField(max_length=256, verbose_name=_(u'title'))
    user = ForeignKey(User, blank=True, null=True, verbose_name=_(u'user'))

    class Meta:
        verbose_name = _(u'vehicle brand')
        verbose_name_plural = _(u'vehicle brands')

    def __unicode__(self):
        return u'{0}'.format(self.title)

    @staticmethod
    def autocomplete_search_fields():
        return ("id__iexact", "title__icontains",)

    @classmethod
    def check_object(cls, brand_id, brand_title, user):
        if brand_id:
            vehicle_brand = cls.get_object(brand_id)
            vehicle_brand.update_object(brand_title, user)
        elif brand_title:
            vehicle_brand = VehicleBrand.objects.create(title=brand_title, user=user)
        return vehicle_brand

    @classmethod
    def get_object(cls, brand_id):
        try:
            return cls.objects.get(id=brand_id)
        except cls.DoesNotExist:
            raise ApiStandartException(dict(error_code=400006))

    def update_object(self, title=None, user=None):
        if self.user == user and title and self.title != title:
            self.title = title
            self.save()


class VehicleType(Model):
    title = CharField(max_length=256, verbose_name=_(u'title'))
    must_registered = BooleanField(default=True, verbose_name=_(u'must be registered'))
    vehicle_brands = ManyToManyField(
        VehicleBrand, related_name=u'vehicle_type_brands', through='VehicleTypeBrand',
        verbose_name=_(u'vehicle type brands')
    )

    class Meta:
        verbose_name = _(u'vehicle type')
        verbose_name_plural = _(u'vehicle types')

    def __unicode__(self):
        return u'{0}'.format(self.title)

    @staticmethod
    def autocomplete_search_fields():
        return ("id__iexact", "title__icontains",)

    @classmethod
    def get_object(cls, vehicle_type_id):
        try:
            vehicle_type = cls.objects.get(id=vehicle_type_id)
        except cls.DoesNotExist:
            raise ApiStandartException(dict(error_code=400005))
        return vehicle_type


class VehicleTypeBrand(Model):
    vehicle_type = ForeignKey(VehicleType, related_name='vehicle_types', verbose_name=_(u'vehicle type'))
    vehicle_brand = ForeignKey(VehicleBrand, related_name='vehicle_brands', verbose_name=_(u'vehicle brand'))

    class Meta:
        verbose_name = _(u'vehicle type brand')
        verbose_name_plural = _(u'vehicle types brands')

    def __unicode__(self):
        return u'type - {0}, brand - {1}'.format(self.vehicle_type.title, self.vehicle_brand.title)


class VehicleModel(Model):
    title = CharField(max_length=512, verbose_name=_(u'title'))
    max_bulk = FloatField(blank=True, null=True, verbose_name=_(u'max bulk'))
    max_capacity = FloatField(blank=True, null=True, verbose_name=_(u'max capacity'))
    user = ForeignKey(User, blank=True, null=True, verbose_name=_(u'owner'))
    vehicle_type_brand = ForeignKey(
        VehicleTypeBrand, related_name='vehicle_type_brands',
        verbose_name=_(u'vehicle type brand')
    )

    class Meta:
        verbose_name = _(u'vehicle model')
        verbose_name_plural = _(u'vehicle models')

    def __unicode__(self):
        return u'vehicle model - {0}'.format(self.title)

    @classmethod
    def check_object(cls, model_id, model_title, user, vehicle_type, vehicle_brand):
        if model_id:
            model = cls.get_object(model_id)
            model.update_object(user, model_title)
        elif model_title:
            vehicle_type_brand, is_created = VehicleTypeBrand.objects.get_or_create(vehicle_type=vehicle_type,
                                                                                    vehicle_brand=vehicle_brand)
            model = VehicleModel.objects.create(title=model_title, user=user,
                                                vehicle_type_brand=vehicle_type_brand)
        return model

    @classmethod
    def get_object(cls, model_id):
        try:
            return cls.objects.get(id=model_id)
        except cls.DoesNotExist:
            raise ApiStandartException(dict(error_code=400007))

    def update_object(self, user, model_title=None):
        if self.user == user and model_title and self.title != model_title:
            self.title = model_title
            self.save()


class Vehicle(Model):
    reg_number = CharField(max_length=512, null=True, blank=True, verbose_name=_(u'registration number'))
    vin_code = CharField(max_length=512, null=True, blank=True, verbose_name=_(u'vin code'))
    bulk = FloatField(null=True, blank=True, verbose_name=_(u'bulk'))
    capacity = FloatField(null=True, blank=True, verbose_name=_(u'capacity'))
    comment = TextField(max_length=2048, null=True, blank=True, verbose_name=_(u'comment'))
    color = ForeignKey(Color, blank=True, null=True, verbose_name=_(u'color'))
    manufacture_year = DateField(verbose_name=_(u'year'))
    vehicle_model = ForeignKey(VehicleModel, related_name='vehicles_models', verbose_name=_(u'vehicle model'))
    user = ForeignKey(User, verbose_name=_(u'user'))
    is_deleted = BooleanField(default=False, verbose_name=_(u'is deleted'))
    created_at = DateTimeField(auto_now_add=True, blank=True, null=True, verbose_name=_(u'created at'))

    class Meta:
        verbose_name = _(u'vehicle')
        verbose_name_plural = _(u'vehicles')

    def __unicode__(self):
        return u'vehicle {0}'.format(self.reg_number)

    def related_label(self):
        return u'vehicle {0}'.format(self.reg_number)

    def get_carts(self):
        return Cart.objects.filter(vehicle=self, user=self.user)

    def get_photos(self, user):
        return Attachment.objects.filter(
            content_type=ContentType.objects.get_for_model(self), object_id=self.id, user=user
        )

    def update_photos(self, photos, user):
        vehicle_photos = self.get_photos(user)

        difference_photos = set(vehicle_photos).difference(photos)

        for photo in difference_photos:
            photo.delete()

        for photo in photos:
            photo.set_content_type(self)

    def update_carts(self, carts):
        vehicle_carts = self.get_carts()

        difference_carts = set(vehicle_carts).difference(carts)
        for cart in difference_carts:
            cart.set_vehicle(None)

        for cart in carts:
            cart.set_vehicle(self)

    @classmethod
    def get_object(cls, vehicle_id, user):
        try:
            return cls.objects.get(pk=vehicle_id, user=user)
        except cls.DoesNotExist:
            raise ApiStandartException(dict(error_code=400013))

    def check_in_transportation(self):
        from orders.models import TransporterTripOrderItem
        if TransporterTripOrderItem.objects.filter(
                is_completed=False,
                date_transporter_trip__transporter_trip__transporter_object_id=self.id,
                date_transporter_trip__transporter_trip__transporter_content_type=ContentType.objects.get_for_model(self),
        ).exists():
            return True
        return False

    def check_in_auction(self):
        from orders.models import Bet
        if Bet.objects.filter(Q(Q(status=0) | Q(status=1)), vehicle=self).exists():
            return True
        return False


class CartType(Model):
    title = CharField(max_length=256, verbose_name=_(u'title'))
    is_active = BooleanField(default=False, verbose_name=_(u'is active'))
    characteristics = ManyToManyField(Characteristic, through='CartTypeCharacteristic',
                                      verbose_name=_(u'cart type characteristics'))

    class Meta:
        verbose_name = _(u'cart type')
        verbose_name_plural = _(u'cart types')

    def __unicode__(self):
        return u'cart type - {0}'.format(self.title)

    @staticmethod
    def autocomplete_search_fields():
        return ("id__iexact", "title__icontains",)

    @classmethod
    def get_for_cart(cls, cart):
        return cls.objects.filter(cart_types__cart_type_characteristics__cart=cart).first()

    @classmethod
    def get_object(cls, cart_type_id):
        try:
            return cls.objects.get(pk=cart_type_id)
        except cls.DoesNotExist:
            raise ApiStandartException(dict(error_code=400012))


class CartTypeCharacteristic(Model):
    characteristic = ForeignKey(Characteristic, related_name='characteristics',
                                verbose_name=_(u'characteristic'))
    cart_type = ForeignKey(CartType, related_name='cart_types', verbose_name=_(u'cart_type'))

    class Meta:
        verbose_name = _(u'cart type characteristic')
        verbose_name_plural = _(u'carts types characteristics')

    def __unicode__(self):
        return u'cart type - {0} characteristic {1}'.format(self.cart_type.title,
                                                            self.characteristic.title)

    @classmethod
    def get_object(cls, cart_type, characteristic):
        try:
            return cls.objects.get(cart_type=cart_type, characteristic=characteristic)
        except cls.DoesNotExist:
            raise ApiStandartException(dict(error_code=400014))


class Cart(Model):
    # 0 - прицеп
    # 1 - полуприцеп
    # 2 - кузов
    BODY_TYPES = (
        (0, _(u'trailer')),
        (1, _(u'semitrailer')),
        (2, _(u'body')),
    )

    reg_number = CharField(max_length=512, blank=True, null=True, verbose_name=_(u'reg number'))
    vin_code = CharField(max_length=512, blank=True, null=True, verbose_name=_(u'vin code'))
    comment = TextField(max_length=2048, blank=True, null=True, verbose_name=_(u'comment'))
    title = CharField(max_length=256, blank=True, null=True, verbose_name=_(u'title'))
    bulk = FloatField(blank=True, null=True, verbose_name=_(u'bulk'))
    capacity = FloatField(blank=True, null=True, verbose_name=_(u'capacity'))
    body_type = PositiveIntegerField(choices=BODY_TYPES, verbose_name=_(u'body type'))
    user = ForeignKey(User, verbose_name=_(u'user'))
    vehicle = ForeignKey(Vehicle, related_name='vehicle_carts', blank=True, null=True,
                         verbose_name=_(u'vehicle'))
    manufacture_year = DateField(blank=True, null=True, verbose_name=_(u'year'))
    is_deleted = BooleanField(default=False, verbose_name=_(u'is deleted'))
    created_at = DateTimeField(auto_now_add=True, blank=True, null=True, verbose_name=_(u'created at'))

    class Meta:
        verbose_name = _(u'cart')
        verbose_name_plural = _(u'carts')

    def __unicode__(self):
        return u'cart - reg_number = {0}'.format(self.reg_number)

    @classmethod
    def get_object(cls, cart_id):
        try:
            return cls.objects.get(id=cart_id)
        except cls.DoesNotExist:
            raise ApiStandartException(dict(error_code=400009))

    def set_vehicle(self, vehicle):
        self.vehicle = vehicle
        self.save()

    def get_characteristics(self):
        return CartCharacteristic.objects.filter(cart=self)

    def get_photos(self, user):
        return Attachment.objects.filter(
            content_type=ContentType.objects.get_for_model(self), object_id=self.id, user=user
        )

    def update_photos(self, photos, user):
        cart_photos = self.get_photos(user)

        difference_photos = set(cart_photos).difference(photos)

        for photo in difference_photos:
            photo.delete()

        for photo in photos:
            photo.set_content_type(self)

    def update_characteristics(self, characteristics):
        # Characteristics item format - {'cart_type_characteristic':obj, 'value':'string'}
        old_cart_characteristics = self.get_characteristics()
        cart_characteristics = list()
        for item in characteristics:
            cart_characteristic, is_created = CartCharacteristic.objects.get_or_create(
                cart=self,
                cart_type_characteristic=item['cart_type_characteristic'],
                defaults=dict(
                    value=item['value']
                )
            )
            if not is_created:
                cart_characteristic.value = item['value']
                cart_characteristic.save()
            cart_characteristics.append(cart_characteristic)

        difference_characteristics = set(old_cart_characteristics).difference(cart_characteristics)
        for item in difference_characteristics:
            item.delete()

    def check_in_transportation(self):
        from orders.models import TransporterTripOrderItem
        if self.vehicle and TransporterTripOrderItem.objects.filter(
                is_completed=False,
                date_transporter_trip__transporter_trip__transporter_object_id=self.vehicle.id,
                date_transporter_trip__transporter_trip__transporter_content_type=ContentType.objects.get_for_model(self),
        ).exists():
            return True
        return False

    def check_in_auction(self):
        from orders.models import Bet
        if Bet.objects.filter(Q(Q(status=0) | Q(status=1)), carts__in=[self.id]).exists():
            return True
        return False


class CartCharacteristic(Model):
    cart = ForeignKey(Cart, related_name='cart_characteristics', verbose_name=_(u'vehicle cart'))
    cart_type_characteristic = ForeignKey(CartTypeCharacteristic, related_name='cart_type_characteristics',
                                          verbose_name=_(u'cart type characteristic'))
    value = CharField(max_length=128, blank=True, null=True, verbose_name=_(u'value'))

    class Meta:
        verbose_name = _(u'cart characteristic')
        verbose_name_plural = _(u'carts characteristics')

    def __unicode__(self):
        return u'cart characteristic {0}'.format(self.value)
