# coding=utf-8

from django.db.models import Q
from rest_framework.response import Response
from rest_framework import status
from rest_framework.viewsets import ModelViewSet
from oauth2_provider.ext.rest_framework import TokenHasReadWriteScope
from rest_framework import permissions
from rest_framework.views import APIView

from .serializers import VehicleSerializer, VehicleShortSerializer, VehicleTypeSerializer, VehicleBrandSerializer, \
    VehicleBrandListSerializer, RequestVehicleModelListSerializer, VehicleModelSerializer, CartShortSerializer, \
    CartDetailSerializer, CartTypeSerializer, CartSerializer, VehicleDetailSerializer, ColorSerializer
from .models import Vehicle, VehicleType, VehicleBrand, VehicleModel, Cart, CartType
from auxiliaries.models import Color
from support.exceptions import ApiStandartException

__author__ = 'sandan'


class VehicleTypeViewSet(ModelViewSet):
    model = VehicleType
    serializer_class = VehicleTypeSerializer
    queryset = VehicleType.objects.all()
    permission_classes = (permissions.IsAuthenticated, TokenHasReadWriteScope)


class VehicleBrandViewSet(ModelViewSet):
    """Vehicle - this is good."""
    model = VehicleBrand
    serializer_class = VehicleBrandSerializer
    queryset = VehicleBrand.objects.all()
    permission_classes = (permissions.IsAuthenticated, TokenHasReadWriteScope)

    def list(self, request, *args, **kwargs):
        if 'vehicle_type_id' in request.query_params:
            serializer = VehicleBrandListSerializer(data=request.query_params, context=dict(request=request))
            serializer.is_valid()
            brands = serializer.validated_data.get('vehicle_type_id').vehicle_brands.all()
        else:
            brands = self.model.objects.filter(
                Q(user=None) | Q(user=request.user)
            )
        return Response(
            self.serializer_class(
                brands,
                many=True
            ).data,
            status=status.HTTP_200_OK
        )


class VehicleModelViewSet(ModelViewSet):
    model = VehicleModel
    serializer_class = VehicleModelSerializer
    queryset = VehicleModel.objects.all()
    permission_classes = (permissions.IsAuthenticated, TokenHasReadWriteScope)

    def list(self, request, *args, **kwargs):
        serializer = RequestVehicleModelListSerializer(data=request.query_params, context=dict(request=request))
        serializer.is_valid()
        return Response(
            self.serializer_class(
                self.model.objects.filter(
                    Q(Q(user=None) | Q(user=request.user)),
                    vehicle_type_brand__vehicle_type=serializer.validated_data.get(
                        'vehicle_type_id'),
                    vehicle_type_brand__vehicle_brand=serializer.validated_data.get(
                        'vehicle_brand_id'),
                ),
                many=True
            ).data,
            status=status.HTTP_200_OK
        )


class VehiclesViewSet(ModelViewSet):
    model = Vehicle
    serializer_class = VehicleSerializer
    queryset = Vehicle.objects.filter(is_deleted=False)
    permission_classes = (permissions.IsAuthenticated, TokenHasReadWriteScope)

    def get_queryset(self):
        return super(VehiclesViewSet, self).get_queryset().filter(user=self.request.user)

    def get_object(self):
        try:
            vehicle = self.model.objects.get(id=self.kwargs['pk'], user=self.request.user)
        except self.model.DoesNotExist:
            raise ApiStandartException(dict(error_code=400010))
        return vehicle

    def list(self, request, *args, **kwargs):
        return Response(VehicleShortSerializer(self.get_queryset(), many=True, context=dict(request=request)).data,
                        status=status.HTTP_200_OK)

    def retrieve(self, request, *args, **kwargs):
        return Response(VehicleDetailSerializer(self.get_object(), context=dict(request=request)).data)

    def create(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data, context=dict(request=request))
        serializer.is_valid()
        vehicle = serializer.save()
        return Response(VehicleDetailSerializer(vehicle, context=dict(request=request)).data)

    def update(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data, context=dict(request=request))
        serializer.is_valid()
        vehicle = serializer.update(self.get_object(), serializer.validated_data)
        return Response(VehicleDetailSerializer(vehicle, context=dict(request=request)).data)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        if instance.check_in_transportation():
            raise ApiStandartException(dict(error_code=400051))
        if instance.check_in_auction():
            raise ApiStandartException(dict(error_code=400052))
        for item in instance.vehicle_carts.all():
            item.vehicle = None
            item.save()
        instance.is_deleted = True
        instance.save()
        return Response(status=status.HTTP_204_NO_CONTENT)


class CartViewSet(ModelViewSet):
    model = Cart
    serializer_class = CartDetailSerializer
    queryset = Cart.objects.filter(is_deleted=False)
    permission_classes = (permissions.IsAuthenticated, TokenHasReadWriteScope)

    def get_queryset(self):
        return self.queryset.filter(
            user=self.request.user
        )

    def list(self, request, *args, **kwargs):
        return Response(CartShortSerializer(self.get_queryset(), many=True, context=dict(request=request)).data,
                        status=status.HTTP_200_OK)

    def retrieve(self, request, *args, **kwargs):
        return Response(self.serializer_class(self.get_object(), context=dict(request=request)).data,
                        status=status.HTTP_200_OK)

    def create(self, request, *args, **kwargs):
        serializer = CartSerializer(data=request.data, context=dict(request=request))
        serializer.is_valid()
        cart = serializer.save()
        return Response(CartDetailSerializer(cart, context=dict(request=request)).data)

    def update(self, request, *args, **kwargs):
        serializer = CartSerializer(data=request.data, context=dict(request=request))
        serializer.is_valid()
        cart = serializer.update(self.get_object(), serializer.validated_data)
        return Response(CartDetailSerializer(cart, context=dict(request=request)).data)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        if instance.check_in_auction():
            raise ApiStandartException(dict(error_code=400053))
        if instance.check_in_transportation():
            raise ApiStandartException(dict(error_code=400054))
        instance.is_deleted = True
        instance.save()
        return Response(status=status.HTTP_204_NO_CONTENT)


class CartTypeView(APIView):
    permission_classes = (permissions.IsAuthenticated, TokenHasReadWriteScope)

    def get(self, request):
        return Response(CartTypeSerializer(CartType.objects.filter(is_active=True), many=True).data)


class DriverView(APIView):
    permission_classes = (permissions.IsAuthenticated, TokenHasReadWriteScope)

    def get(self, request):
        #todo driver page in web panel
        return Response()

