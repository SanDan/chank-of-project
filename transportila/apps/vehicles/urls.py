from django.conf.urls import patterns, url, include
from rest_framework import routers

from .api import VehiclesViewSet, VehicleTypeViewSet, VehicleBrandViewSet, VehicleModelViewSet, CartViewSet, \
    CartTypeView, DriverView

v1_router = routers.SimpleRouter()
v1_router.register(r'vehicles', VehiclesViewSet)
v1_router.register(r'vehicles-types', VehicleTypeViewSet)
v1_router.register(r'vehicles-brands', VehicleBrandViewSet)
v1_router.register(r'vehicles-models', VehicleModelViewSet)
v1_router.register(r'carts', CartViewSet)

v1_urlpatterns = [
    url(r'carts/types/$', CartTypeView.as_view()),
    url(r'drivers/(?P<nickname>[\w-]+)/$', DriverView.as_view()),
    url(r'', include(v1_router.urls))
]


urlpatterns = [
    url(r'^api/v1/', include(v1_urlpatterns)),
]
