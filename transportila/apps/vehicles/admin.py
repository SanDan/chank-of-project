from django.contrib import admin
from django.contrib.admin.decorators import register
from genericadmin.admin import GenericAdminModelAdmin, GenericTabularInline
from modeltranslation.admin import TranslationAdmin

from .models import VehicleBrand, VehicleType, VehicleTypeBrand, VehicleModel, Color, Vehicle, \
    CartType, CartTypeCharacteristic, Cart, CartCharacteristic
from auxiliaries.admin import InlineAttachment


@register(VehicleBrand)
class AdminVehicleBrand(admin.ModelAdmin):
    list_display = ['title', 'user']


@register(VehicleType)
class AdminVehicleType(admin.ModelAdmin):
    list_display = ['title']
    exclude = ['title_en']


@register(VehicleTypeBrand)
class AdminVehicleTypeBrand(admin.ModelAdmin):
    list_display = ['vehicle_type', 'vehicle_brand']
    raw_id_fields = ('vehicle_type', 'vehicle_brand')
    autocomplete_lookup_fields = {
        'fk': ['vehicle_type', 'vehicle_brand'],
        'm2m': []
    }

    class Media:
        js = (
            'admin/js/django-admin-sortable.js',
            'admin/js/rel'
        )

        css = {
            'all': ('admin/css/admin-sortable-fix.css',)
        }

    def get_queryset(self, request):
        return super(AdminVehicleTypeBrand, self).get_queryset(request).select_related('vehicle_type', 'vehicle_brand')


@register(VehicleModel)
class AdminVehicleModel(admin.ModelAdmin):
    list_display = ['title', 'max_bulk', 'max_capacity', 'user', 'vehicle_type_brand']
    raw_id_fields = ()
    autocomplete_lookup_fields = {
        'fk': [],
        'm2m': []
    }


@register(Vehicle)
class AdminVehicle(GenericAdminModelAdmin):
    list_display = ['reg_number', 'vin_code', 'id', 'vehicle_model', 'color', 'is_deleted']
    inlines = [InlineAttachment, ]
    # raw_id_fields = ('vehicle_model', 'user',)
    # autocomplete_lookup_fields = {
    #     'fk': ['vehicle_model', 'user'],
    #     'm2m': []
    # }


@register(CartType)
class AdminCartType(TranslationAdmin):
    list_display = ['title']


@register(CartTypeCharacteristic)
class AdminCartTypeCharacteristic(admin.ModelAdmin):
    list_display = ['characteristic', 'cart_type']


@register(CartCharacteristic)
class AdminCartCharacteristic(admin.ModelAdmin):
    list_display = ['value', 'cart_type_characteristic', 'cart']


@register(Cart)
class AdminCart(GenericAdminModelAdmin):
    list_display = ['reg_number', 'vin_code', 'vehicle', 'is_deleted']
    raw_id_fields = ()
    autocomplete_lookup_fields = {
        'fk': [],
        'm2m': [],
    }
    inlines = [InlineAttachment, ]








