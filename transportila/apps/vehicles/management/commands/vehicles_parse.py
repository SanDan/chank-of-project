# coding=utf-8
__author__ = 'Slyfest'

import os
import json
from django.core.management.base import BaseCommand
from vehicles.models import VehicleBrand, VehicleModel, VehicleType, VehicleTypeBrand

cars_file_path = os.path.join(os.path.dirname(__file__), 'data', 'cars.json')
trucks_file_path = os.path.join(os.path.dirname(__file__), 'data', 'trucks.json')
scooter_file_path = os.path.join(os.path.dirname(__file__), 'data', 'moto.json')
moto_file_path = os.path.join(os.path.dirname(__file__), 'data', 'mototest.json')


def add_car(car_obj):
    vehicle_type, is_created = VehicleType.objects.get_or_create(
        title=car_obj['type'].strip(),
        title_ru=car_obj['type'].strip()
    )
    vehicle_brand, is_created = VehicleBrand.objects.get_or_create(
        title=car_obj['mark'][0].strip()
    )
    vehicle_type_brand, is_created = VehicleTypeBrand.objects.get_or_create(
        vehicle_type=vehicle_type,
        vehicle_brand=vehicle_brand
    )
    for model in car_obj['model']:
        VehicleModel.objects.get_or_create(
            title=model.strip(),
            vehicle_type_brand=vehicle_type_brand
        )


def add_moto(car_obj):
    vehicle_type, is_created = VehicleType.objects.get_or_create(
        title=car_obj['type'].strip(),
        title_ru=car_obj['type'].strip()
    )
    vehicle_brand, is_created = VehicleBrand.objects.get_or_create(
        title=car_obj['mark'][0].strip()
    )
    vehicle_type_brand, is_created = VehicleTypeBrand.objects.get_or_create(
        vehicle_type=vehicle_type,
        vehicle_brand=vehicle_brand
    )
    VehicleModel.objects.get_or_create(
        title=car_obj['model'].strip(),
        vehicle_type_brand=vehicle_type_brand
    )

class Command(BaseCommand):
    args = '<>'
    help = 'Add transport data'

    def handle(self, *args, **options):
        cars = list()
        with open(cars_file_path) as json_file:
            data = json.load(json_file)

        for entry in data:
            car = dict(
                model=list(entry['model']),
                mark=entry['mark'],
                type='Легковые автомобили'
            )
            cars.append(car)

        for car in cars:
            add_car(car)
            print u'{0}:    done'.format(car['model'])

        trucks = list()
        with open(trucks_file_path) as json_file:
            data = json.load(json_file)

        for entry in data:
            truck = dict(
                model=list(entry['model']),
                mark=entry['mark'],
                type='Грузовые автомобили'
            )
            trucks.append(truck)

        for truck in trucks:
            add_car(truck)
            print u'{0}:    done'.format(truck['model'])

        scooters = list()
        with open(scooter_file_path) as json_file:
            data = json.load(json_file)

        for entry in data:
            scooter = dict(
                model=list(entry['model']),
                mark=entry['mark'],
                type='Мототехника'
            )
            scooters.append(scooter)

        for scooter in scooters:
            add_car(scooter)
            print u'{0}:    done'.format(scooter['model'])

        motos = list()
        with open(moto_file_path) as json_file:
            data = json.load(json_file)

        for entry in data:
            moto = dict(
                model=entry['model'],
                mark=entry['mark'],
                type='Мототехника',
            )
            motos.append(moto)

        for moto in motos:
            add_moto(moto)
            print u'{0}:    done'.format(moto['model'])
