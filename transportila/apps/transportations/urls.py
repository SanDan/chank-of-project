from django.conf.urls import patterns, url, include
from rest_framework import routers

from .api import ScanTransportationCargoItemViewSet, TransportationViewSet

__author__ = 'sandan'

v1_transporter_router = routers.SimpleRouter()
v1_transporter_router.register('transportations', TransportationViewSet)
v1_transporter_router.register('cargoes/transfer', ScanTransportationCargoItemViewSet)

v1_urlpatterns = [
    url(r'transporter/', include(v1_transporter_router.urls)),
]


urlpatterns = [
    url(r'^api/v1/', include(v1_urlpatterns)),
]

