# coding=utf-8
import uuid
from datetime import timedelta, datetime
from time import time, sleep
import subprocess
import os

from django.conf import settings
from django.db.models import Model, ForeignKey, PositiveIntegerField, CharField, DateTimeField, UUIDField, BooleanField
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.utils.translation import ugettext_lazy as _
from django.template.loader import render_to_string

from payments.models import Currency
from accounts.models import CustomUser
from profiles.models import BasicInfo
from orders.models import CategoryCargoCharacteristic, OrderItemPoint, OrderItem, OrderItemCategoryCargoCharacteristic
from support.exceptions import ApiStandartException
from support.route import get_route_g
from auxiliaries.models import Attachment
from support.send_email_pdf import send_email_pdf
from queue.send_email import SendEmail


class TransferPoint(Model):
    point_content_type = ForeignKey(ContentType, related_name='trip_point', blank=True, null=True,
                                    verbose_name=_(u'point content type'))
    point_object_id = PositiveIntegerField(blank=True, null=True,
                                           verbose_name=_(u'point object id'))
    point_content_object = GenericForeignKey('point_content_type', 'point_object_id')

    comment = CharField(max_length=2048, blank=True, null=True, verbose_name=_(u'comment'))
    created_at = DateTimeField(auto_now_add=True, verbose_name=_(u'created at'))

    class Meta:
        verbose_name = _(u'transfer point')
        verbose_name_plural = _(u'transfer points')

    def __unicode__(self):
        return u'trip point - {0}'.format(self.id)


class Trip(Model):
    number = CharField(max_length=256, blank=True, null=True, verbose_name=_(u'number'))
    created_at = DateTimeField(auto_now_add=True, verbose_name=_(u'created at'))

    class Meta:
        verbose_name = _(u'trip')
        verbose_name_plural = _(u'trips')

    def __unicode__(self):
        return u'trip id - {0}, number - {1}'.format(self.id, self.number)


class TransferPointTrip(Model):
    trip = ForeignKey(Trip, related_name='transfer_point_trips', verbose_name=_(u'trip'))
    transfer_point = ForeignKey(TransferPoint, related_name='transfer_points_trip', verbose_name=_(u'transfer point'))
    parent = ForeignKey('self', related_name='parent_transfer_point_trip', blank=True, null=True,
                        verbose_name=_(u'parent'))
    child = ForeignKey('self', related_name='child_transfer_point_trip', blank=True, null=True,
                       verbose_name=_(u'child'))
    distance = PositiveIntegerField(blank=True, null=True, verbose_name=_(u'distance'))
    route = CharField(blank=True, null=True, max_length=4096, verbose_name=u'route polyline')

    created_at = DateTimeField(auto_now_add=True, verbose_name=_(u'created at'))

    class Meta:
        verbose_name = _(u'transfer point trip')
        verbose_name_plural = _(u'transfer points trips')

    def __unicode__(self):
        return u'transfer point trip - {0}'.format(self.id)

    def set_child(self, child):
        self.child = child
        self.save()

    def set_parent(self, parent):
        self.parent = parent
        self.save()


class Schedule(Model):
    title = CharField(max_length=256, verbose_name=_(u'title'))

    class Meta:
        verbose_name = _(u'schedule')
        verbose_name_plural = _(u'schedules')

    def __unicode__(self):
        return u'schedule - {0}'.format(self.title)


class ScheduleTrip(Model):
    arrival_at = DateTimeField(verbose_name=_(u'arrival at'))
    depart_at = DateTimeField(blank=True, null=True, verbose_name=_(u'depart at'))
    transfer_point_trip = ForeignKey(TransferPointTrip, related_name='transfer_point_trip_schedule_trips',
                                     verbose_name=_(u'transfer point trip'))
    schedule = ForeignKey(Schedule, blank=True, null=True, verbose_name=_(u'schedule'))
    created_at = DateTimeField(auto_now_add=True, verbose_name=_(u'created at'))

    class Meta:
        verbose_name = _(u'schedule trip')
        verbose_name_plural = _(u'schedules trips')

    def __unicode__(self):
        return u'schedule arrival - {0}, depart - {1}'.format(self.arrival_at, self.depart_at)


class TransporterTrip(Model):
    transporter_content_type = ForeignKey(ContentType, related_name='transporter_content_type', blank=True, null=True,
                                          verbose_name=_(u'transporter content type'))
    transporter_object_id = PositiveIntegerField(blank=True, null=True,
                                                 verbose_name=_(u'transporter object id'))
    transporter_content_object = GenericForeignKey('transporter_content_type', 'transporter_object_id')
    trip = ForeignKey(Trip, related_name='trips', blank=True, null=True, verbose_name=_(u'trip'))
    created_at = DateTimeField(auto_now_add=True, verbose_name=_(u'created at'))

    class Meta:
        verbose_name = _(u'transporter trip')
        verbose_name_plural = _(u'transporters trips')

    def __unicode__(self):
        return u'transporter trip {0}'.format(self.id)

    def check_state(self, user):
        # method for checking permissions. return transporter_type
        if self.transporter_content_type == 'customuser':
            if self.transporter_content_object != user:
                raise ApiStandartException(dict(error_code=400056))
            return 'user'
        elif self.transporter_content_type == 'vehicle':
            if self.transporter_content_object.user != user:
                raise ApiStandartException(dict(error_code=400056))
            return 'vehicle'


class Transportation(Model):
    STATUSES = (
        (0, _(u'on the way')),
        (1, _(u'unloading and (or) loading')),
        (2, _(u'failed')),
        (3, _(u'canceled')),
        (4, _(u'done')),
    )

    uuid = UUIDField(primary_key=True, db_index=True, unique=True, verbose_name=_(u'uuid'))
    status = PositiveIntegerField(default=0, choices=STATUSES, verbose_name=_(u'status'))
    transporter_trip = ForeignKey(TransporterTrip, related_name='transportation_transporter_trip',
                                  verbose_name=_(u'transporter trip'))
    order_content_type = ForeignKey(ContentType, related_name='order_content_type',
                                    verbose_name=_(u'order content type'))
    order_object_id = CharField(max_length=256, verbose_name=_(u'order object id'))
    order_content_object = GenericForeignKey('order_content_type', 'order_object_id')

    created_at = DateTimeField(auto_now_add=True, verbose_name=_(u'created at'))

    class Meta:
        verbose_name = _(u'transportation')
        verbose_name_plural = _(u'transportation')

    def __unicode__(self):
        return u'transportation - {0}'.format(self.uuid)

    @staticmethod
    def get_approximate_time_on_road(start_date, start_lat, start_lng, destination_lat, destination_lng):
        distance = get_route_g(start_lat, start_lng, destination_lat, destination_lng)['distance']
        speed = 60
        time_without_sleep = distance / 1000 / speed
        return start_date + timedelta(hours=int(time_without_sleep + int(time_without_sleep / 24) * 8))

    @classmethod
    def making_from_customer_order_for_single_transporter(cls, transporter, order, date_arrival_acceptance):
        from queue.send_certificate import SendingTransportationCertificates
        order_items = list(order.order_items.all())

        transporter_trip = TransporterTrip.objects.create(
            transporter_object_id=transporter.pk,
            transporter_content_type=ContentType.objects.get_for_model(transporter)
        )

        transportation = Transportation.objects.create(
            uuid=uuid.uuid4(),
            transporter_trip=transporter_trip,
            order_object_id=order.pk,
            order_content_type=ContentType.objects.get_for_model(order)
        )

        transfer_point_acceptance = None
        point_acceptance = order_items[0].point_acceptance
        if point_acceptance.point_content_type.model.lower() == 'address':
            transfer_point_acceptance, is_created = TransferPoint.objects.get_or_create(
                point_object_id=point_acceptance.point_content_object.id,
                point_content_type=ContentType.objects.get_for_model(point_acceptance.point_content_object)
            )
            start_lat = point_acceptance.point_content_object.address_coord.latitude
            start_lng = point_acceptance.point_content_object.address_coord.longitude

        transfer_point_delivery = None
        point_delivery = order_items[0].point_delivery
        if point_delivery.point_content_type.model.lower() == 'address':
            transfer_point_delivery, is_created = TransferPoint.objects.get_or_create(
                point_object_id=point_delivery.point_content_object.id,
                point_content_type=ContentType.objects.get_for_model(point_delivery.point_content_object)
            )
            destination_lat = point_acceptance.point_content_object.address_coord.latitude
            destination_lng = point_acceptance.point_content_object.address_coord.longitude

        if not (transfer_point_acceptance and transfer_point_delivery):
            raise ApiStandartException(dict(error_code=400048))

        directions = get_route_g(start_lat, start_lng, destination_lat, destination_lng)

        if 'distance' not in directions or 'route' not in directions:
            directions['distance'] = None
            directions['route'] = None
        transportation_transfer_point_acceptance = TransportationTransferPoint.objects.create(
            uuid=uuid.uuid4(),
            transportation=transportation,
            transfer_point=transfer_point_acceptance,
            distance=directions['distance'],
            route=directions['route']
        )
        transportation_transfer_point_delivery = TransportationTransferPoint.objects.create(
            uuid=uuid.uuid4(),
            transportation=transportation,
            transfer_point=transfer_point_delivery,
            parent=transportation_transfer_point_acceptance
        )
        transportation_transfer_point_acceptance.set_child(transportation_transfer_point_delivery)

        if order_items[0].point_delivery.date_transfer.order_date_type == 1:
            date_transfer = order_items[0].point_acceptance.date_transfer
            date_arrival_delivery = date_transfer.first_date + timedelta(
                seconds=int((date_transfer.second_date - date_transfer.first_date).total_seconds() / 2))
        else:
            date_arrival_delivery = order_items[0].point_delivery.date_transfer.first_date

        TransportationSchedule.objects.create(
            transportation_transfer_point=transportation_transfer_point_acceptance,
            arrival_at=date_arrival_acceptance,
            type_check=TransportationSchedule.TYPES_CHECK[2][0]
        )

        TransportationSchedule.objects.create(
            transportation_transfer_point=transportation_transfer_point_delivery,
            arrival_at=date_arrival_delivery,
            type_check=TransportationSchedule.TYPES_CHECK[2][0]
        )

        for item in order_items:
            acceptance_point = TrusteeTransportationPoint.objects.create(
                uuid=uuid.uuid4(),
                trustee_object_id=item.point_acceptance.trustee_object_id,
                trustee_content_type=item.point_acceptance.trustee_content_type,
                transfer_point=transfer_point_acceptance
            )

            delivery_point = TrusteeTransportationPoint.objects.create(
                uuid=uuid.uuid4(),
                trustee_object_id=item.point_delivery.trustee_object_id,
                trustee_content_type=item.point_delivery.trustee_content_type,
                transfer_point=transfer_point_delivery
            )
            transportation_cargo_item = TransportationCargoItem.objects.create(
                uuid=uuid.uuid4(),
                cargo_item_object_id=item.uuid,
                cargo_item_content_type=ContentType.objects.get_for_model(item),
                acceptance_point_object_id=acceptance_point.uuid,
                acceptance_point_content_type=ContentType.objects.get_for_model(acceptance_point),
                delivery_point_object_id=delivery_point.uuid,
                delivery_point_content_type=ContentType.objects.get_for_model(delivery_point),
                transportation=transportation
            )
            for cargo_characteristic in list(item.order_item_characteristics.all()):
                TransportationCargoItemCategoryCargoCharacteristic.objects.create(
                    transportation_cargo_item=transportation_cargo_item,
                    category_cargo_characteristic=cargo_characteristic.category_cargo_characteristic,
                    stated_value=cargo_characteristic.value
                )
            item.set_waiting_dispatch_status()

        order.set_carried_out_status()
        # transportation.generate_certificates()
        SendingTransportationCertificates.send.delay(transportation.uuid)

    @classmethod
    def get_by_pk(cls, pk):
        try:
            return cls.objects.select_related('transporter_trip').get(pk=pk)
        except cls.DoesNotExist:
            raise ApiStandartException(dict(error_code=400060))

    def set_transfer_status(self):
        self.status = 1
        self.save()

    def is_transfer_status(self):
        if self.status == 1:
            return True
        return False

    def set_on_the_way_status(self):
        self.status = 0
        self.save()

    def is_on_the_way_status(self):
        if self.status == 0:
            return True
        return False

    def set_done_status(self):
        self.status = 4
        self.save()

    def push_point_cargo(self, cargo, point, point_cargoes, action):
        # point_cargoes item format:
        # {"transfer_point":obj, "trustee":obj, "cargoes":[obj,...]}
        # action: 0 - acceptance, 1 - delivery
        if action == 0:
            action = _(u'acceptance')
        elif action == 1:
            action = _(u'delivery')
        trustee = None
        transfer_point = None
        if type(point).__name__.lower() == 'trusteetransportationpoint':
            if point.is_completed:
                return
            trustee = point.trustee_content_object
            transfer_point = TransportationTransferPoint.objects.prefetch_related(
                'transportation_transfer_point_schedules').get(
                transfer_point=point.transfer_point, transportation=self)
        in_list = False
        for index, item in enumerate(point_cargoes):
            if item['transfer_point'] == transfer_point and item['trustee'] == trustee:
                in_list = True
                break

        if in_list:
            point_cargoes[index]['cargoes'].append(
                dict(cargo=cargo, action=action, transfer_uuid=point.pk)
            )
        else:
            point_cargoes.append(
                dict(
                    transfer_point=transfer_point,
                    trustee=trustee,
                    cargoes=[dict(cargo=cargo, action=action, transfer_uuid=point.pk)]
                )
            )

    def generate_certificates(self):
        cargoes = list(TransportationCargoItem.objects.select_related(
            'transportation',
            'transportation__transporter_trip', 'transportation__transporter_trip'
        ).prefetch_related(
            'transportation_cargo_item_characteristics',
            'transportation_cargo_item_characteristics__category_cargo_characteristic',
            'transportation_cargo_item_characteristics__category_cargo_characteristic__characteristic',
            'transportation_cargo_item_characteristics__category_cargo_characteristic__characteristic__unit',
        ).filter(transportation=self))

        transporter = None
        if self.transporter_trip.transporter_content_type.model.lower() == 'customuser':
            transporter = self.transporter_trip.transporter_content_object
        elif self.transporter_trip.transporter_content_type.model.lower() == 'vehicle':
            transporter = self.transporter_trip.transporter_content_object.user

        if transporter:
            basic_info = transporter.basic_info_profiles.first()
            if basic_info:
                transporter = dict(
                    full_name=basic_info.get_full_name(),
                    phone=basic_info.phone
                )

        point_cargoes = list()
        for item in cargoes:
            if item.acceptance_point_content_type.model.lower() == 'trusteetransportationpoint':
                self.push_point_cargo(item, item.acceptance_point_content_object, point_cargoes, 0)

            if item.delivery_point_content_type.model.lower() == 'trusteetransportationpoint':
                self.push_point_cargo(item, item.delivery_point_content_object, point_cargoes, 1)

        certificates = list()
        for item in point_cargoes:
            certificate = self.generate_certificate_to_trustee_on_point(
                transporter,
                item['trustee'],
                item['transfer_point'],
                item['cargoes'],
                self
            )
            certificates.append(certificate)

        email = None
        if self.order_content_type.model.lower() == 'order':
            email = self.order_content_object.customer.email
        if email:
            send_email_pdf(
                dict(
                    subject_template=settings.CUSTOMER_CERTIFICATE_LETTER_SUBJECT,
                    template_text=settings.CUSTOMER_CERTIFICATE_LETTER_TXT,
                    template_html=settings.CUSTOMER_CERTIFICATE_LETTER_HTML,
                    pdf_paths=certificates,
                    from_email=settings.DEFAULT_FROM_EMAIL,
                    emails_to=[email],
                    params=dict(
                        file_server_url=settings.FILE_SERVER_URL
                    )
                )
            )

        for item in certificates:
            os.remove(item)

    @classmethod
    def generate_certificate_to_trustee_on_point(cls, transporter, trustee, transportation_transfer_point, cargoes, transportation, size=480):
        email = None
        full_name = None
        phone = None
        social_id = None
        if type(trustee).__name__.lower() == 'trustee':
            email = trustee.email
            full_name = u'{0} {1}'.format(trustee.first_name, trustee.last_name)
            phone = trustee.phone
            social_id = trustee.social_id
        elif type(trustee).__name__.lower() == 'customuser':
            basic_info = trustee.basic_info_profiles.first()
            document_info = trustee.user_document_info.first()
            email = trustee.email
            if basic_info and document_info:
                full_name = basic_info.get_full_name()
                phone = basic_info.phone
                social_id = document_info.passport_id if document_info else None
        else:
            raise ApiStandartException(dict(error_code=400069))
        trustee = dict(
            email=email,
            full_name=full_name,
            phone=phone,
            social_id=social_id
        )

        point = dict(
            arrival_at=transportation_transfer_point.transportation_transfer_point_schedules.first().arrival_at
        )
        if transportation_transfer_point.transfer_point.point_content_type.model.lower() == 'address':
            address = transportation_transfer_point.transfer_point.point_content_object
            point['address'] = address.title
            point['latitude'] = address.address_coord.latitude
            point['longitude'] = address.address_coord.longitude

        trustee_cert = render_to_string(settings.TRUSTEE_CERTIFICATE_HTML, context=dict(
            size=size,
            cargoes=cargoes,
            point=point,
            trustee=trustee,
            transporter=transporter
        ))

        return cls.send_certificate(trustee_cert, trustee['email'], transportation)

    @staticmethod
    def generate_certificate(trustee_cert):
        file_name = u'certificate-{0}'.format(int(time()))
        template_path = os.path.join('/tmp', u'{0}.html'.format(file_name))
        template_file = open(template_path, 'w+b')
        template_file.write(trustee_cert.encode('UTF-8'))
        template_file.close()
        pdf_path = os.path.join('/tmp', u'{0}.pdf'.format(file_name))
        retcode = subprocess.call('html-pdf {0} {1}'.format(template_path, pdf_path),
                        shell=True)
        sleep(2)
        return template_path, pdf_path

    @classmethod
    def send_certificate(cls, trustee_cert, email, transportation):
        template_path, pdf_path = cls.generate_certificate(trustee_cert)
        if transportation.order_content_type.model.lower() == 'order' and transportation.order_content_object.customer.email != email:
            send_email_pdf(
                dict(
                    subject_template=settings.TRUSTEE_CERTIFICATE_LETTER_SUBJECT,
                    template_text=settings.TRUSTEE_CERTIFICATE_LETTER_TXT,
                    template_html=settings.TRUSTEE_CERTIFICATE_LETTER_HTML,
                    pdf_paths=[pdf_path],
                    from_email=settings.DEFAULT_FROM_EMAIL,
                    emails_to=[email],
                    params=dict(
                        file_server_url=settings.FILE_SERVER_URL
                    )
                )
            )
        return pdf_path

    @classmethod
    def get_actual_statuses(cls):
        return [0, 1]


class TransportationTransferPoint(Model):
    uuid = UUIDField(primary_key=True, db_index=True, unique=True, verbose_name=_(u'uuid'))
    parent = ForeignKey('self', related_name='transportation_transfer_point_parent', blank=True, null=True,
                        verbose_name=_(u'parent'))
    child = ForeignKey('self', related_name='transportation_transfer_point_child', blank=True, null=True,
                       verbose_name=_(u'child'))
    distance = PositiveIntegerField(blank=True, null=True, verbose_name=_(u'distance'))
    route = CharField(max_length=4096, blank=True, null=True, verbose_name=_(u'route'))
    transportation = ForeignKey(Transportation, related_name='transportation_transfer_points',
                                verbose_name=_(u'transportation'))
    transfer_point = ForeignKey(TransferPoint, related_name='transportations_transfer_point',
                                verbose_name=_(u'transfer point'))
    transfer_point_trip = ForeignKey(TransferPointTrip, blank=True, null=True, verbose_name=_(u'transfer point trip'))
    schedule_trip = ForeignKey(ScheduleTrip, blank=True, null=True, verbose_name=_(u'schedule trip'))
    is_completed = BooleanField(default=False, verbose_name=_(u'is completed'))
    created_at = DateTimeField(auto_now_add=True, verbose_name=_(u'created at'))

    class Meta:
        verbose_name = _(u'transportation transfer point')
        verbose_name_plural = _(u'transportations transfer points')

    def __unicode__(self):
        return u'transportation transfer point {0}, transportation - {1}, transfer point - {2}'.format(
            self.uuid, self.transportation, self.transfer_point)

    def set_child(self, child):
        self.child = child
        self.save()

    def set_parent(self, parent):
        self.parent = parent
        self.save()

    def check_permissions(self, user):
        if self.transportation.transporter_trip.transporter_content_type == 'customuser':
            if self.transportation.transporter_trip.transporter_content_object == user:
                return
        elif self.transportation.transporter_trip.transporter_content_type == 'vehicle':
            if self.transportation.transporter_trip.transporter_content_object.user == user:
                return
        raise ApiStandartException(dict(error_code=400056))


class TransportationSchedule(Model):
    TYPES_CHECK = (
        (0, _(u'temp')),
        (1, _(u'fact')),
        (2, _(u'system'))
    )
    transportation_transfer_point = ForeignKey(
        TransportationTransferPoint, related_name='transportation_transfer_point_schedules',
        verbose_name=_(u'transportation transfer point')
    )
    depart_at = DateTimeField(blank=True, null=True, verbose_name=_(u'fact depart at'))
    arrival_at = DateTimeField(blank=True, null=True, verbose_name=_(u'fact arrival at'))
    version = PositiveIntegerField(default=0, verbose_name=_(u'version'))
    type_check = PositiveIntegerField(default=0, choices=TYPES_CHECK, verbose_name=_(u'type check'))
    updated_user = ForeignKey(CustomUser, blank=True, null=True, verbose_name=_(u'updated user'))
    created_at = DateTimeField(auto_now_add=True, verbose_name=_(u'created at'))

    class Meta:
        verbose_name = _(u'transportation schedule')
        verbose_name_plural = _(u'transportation schedules')
        ordering = ['-version']

    def __unicode__(self):
        return u'transportation schedule - {0}'.format(self.id)

    @classmethod
    def set_fact_arrival_datetime(cls, transportation_transfer_point):
        cls.objects.create(
            transportation_transfer_point=transportation_transfer_point,
            type_check=1,
            arrival_at=datetime.utcnow()
        )

    @classmethod
    def set_fact_depart_datetime(cls, transportation_transfer_point):
        instance = cls.objects.filter(
            transportation_transfer_point=transportation_transfer_point,
            type_check=1
        ).first()
        if instance:
            instance.depart_at = datetime.utcnow()
            instance.save()


class TrusteeTransportationPoint(Model):
    uuid = UUIDField(primary_key=True, unique=True, db_index=True, verbose_name=_(u'uuid'))
    transfer_point = ForeignKey(TransferPoint, verbose_name=_(u'transfer point'))
    trustee_content_type = ForeignKey(ContentType, related_name='trustee_transportation_point', blank=True, null=True,
                                      verbose_name=_(u'trustee content type'))
    trustee_object_id = PositiveIntegerField(blank=True, null=True,
                                             verbose_name=_(u'trustee object id'))
    trustee_content_object = GenericForeignKey('trustee_content_type', 'trustee_object_id')
    transporter_comment = CharField(max_length=2048, blank=True, null=True, verbose_name=_(u'transporter comment'))

    is_completed = BooleanField(default=False, verbose_name=_(u'is completed'))
    completed_at = DateTimeField(blank=True, null=True, verbose_name=_(u'completed at'))
    created_at = DateTimeField(auto_now_add=True, verbose_name=_(u'created at'))

    class Meta:
        verbose_name = _(u'trustee transportation point')
        verbose_name_plural = _(u'trustee transportation points')

    def __unicode__(self):
        return u'trustee transportation point - {0}'.format(self.pk)

    def get_short_trustee_data(self):
        trustee_type = None
        for item in OrderItemPoint.TRUSTEE_TYPES:
            if item[1].lower() == self.trustee_content_type.model.lower():
                trustee_type = item[0]
        return dict(trustee_id=self.trustee_object_id, trustee_type=trustee_type)

    def get_full_trustee_data(self, transportation):
        from orders.serializers import TrusteeSerializer
        from accounts.serializers import UserPublicInfoSerializer
        trustee_type = None
        trustee_data = None
        for item in OrderItemPoint.TRUSTEE_TYPES:
            if item[1].lower() == self.trustee_content_type.model.lower():
                trustee_type = item[0]

        if trustee_type == 0:
            if transportation.order_content_type.model.lower() == 'order':
                trustee_data = UserPublicInfoSerializer(transportation.order_content_object.customer).data
        elif trustee_type == 1:
            trustee_data = TrusteeSerializer(self.trustee_content_object).data
        return dict(
            trustee_type=trustee_type,
            trustee_data=trustee_data
        )

    def set_transporter_comment(self, comment):
        self.transfer_comment = comment
        self.save()

    def get_photos(self, user):
        return Attachment.objects.filter(
            content_type=ContentType.objects.get_for_model(self), object_uuid=self.pk, user=user
        )

    def update_photos(self, photos, user):
        transportation_point_photos = self.get_photos(user)

        difference_photos = set(transportation_point_photos).difference(photos)

        for photo in difference_photos:
            photo.delete()

        for photo in photos:
            photo.set_uuid_content_type(self)

    def set_completed(self):
        self.is_completed = True
        self.save()


class TruckingCompanyTransportationPoint(Model):
    track_number = CharField(max_length=256, verbose_name=_(u'track number'))
    company = CharField(max_length=256, verbose_name=_(u'company'))
    is_completed = BooleanField(default=False, verbose_name=_(u'is completed'))
    completed_at = DateTimeField(blank=True, null=True, verbose_name=_(u'completed at'))
    created_at = DateTimeField(auto_now_add=True, verbose_name=_(u'created at'))

    class Meta:
        verbose_name = _(u'trucking company transportation')
        verbose_name_plural = _(u'trucking companies transportation')

    def __unicode__(self):
        return u'trucking company transportation - {0}'.format(self.id)


class TransportationCargoItem(Model):
    STATUSES = (
        (0, _(u'pending')),
        (1, _(u'picking up')),
        (2, _(u'on board')),
        (3, _(u'transfer')),
        (4, _(u'delivered')),
    )
    uuid = UUIDField(primary_key=True, unique=True, db_index=True, verbose_name=_(u'uuid'))
    cargo_item_content_type = ForeignKey(ContentType, blank=True, null=True,
                                         verbose_name=_(u'cargo item content type'))
    cargo_item_object_id = CharField(max_length=256, blank=True, null=True,
                                     verbose_name=_(u'cargo item object id'))
    cargo_item_content_object = GenericForeignKey('cargo_item_content_type', 'cargo_item_object_id')

    status = PositiveIntegerField(default=0, choices=STATUSES, verbose_name=_(u'status'))
    parent = ForeignKey('self', blank=True, null=True, related_name='transportation_cargo_item_parent',
                        verbose_name=_(u'parent'))
    child = ForeignKey('self', blank=True, null=True, related_name='transportation_cargo_item_child',
                       verbose_name=_(u'child'))

    acceptance_point_content_type = ForeignKey(ContentType, related_name='acceptance_point', blank=True, null=True,
                                               verbose_name=_(u'trustee acceptance content type'))
    acceptance_point_object_id = CharField(max_length=256, blank=True, null=True,
                                           verbose_name=_(u'trustee acceptance object id'))
    acceptance_point_content_object = GenericForeignKey('acceptance_point_content_type',
                                                        'acceptance_point_object_id')

    delivery_point_content_type = ForeignKey(ContentType, related_name='delivery_point', blank=True, null=True,
                                             verbose_name=_(u'trustee delivery content type'))
    delivery_point_object_id = CharField(max_length=256, blank=True, null=True,
                                         verbose_name=_(u'trustee delivery object id'))
    delivery_point_content_object = GenericForeignKey('delivery_point_content_type', 'delivery_point_object_id')

    transportation = ForeignKey(Transportation, related_name='transportation_cargo_items',
                                verbose_name=_(u'transportation'))
    created_at = DateTimeField(auto_now_add=True, verbose_name=_(u'created at'))

    class Meta:
        verbose_name = _(u'transportation cargo item')
        verbose_name_plural = _(u'transportation cargo items')

    def __unicode__(self):
        return u'transportation cargo item - {0}'.format(self.uuid)

    def get_point(self):
        if self.status in [0, 1]:
            return self.acceptance_point_content_object
        elif self.status in [2, 3]:
            return self.delivery_point_content_object

    def check_permissions(self, user):
        if self.transportation.transporter_trip.transporter_content_type.model.lower() == 'customuser':
            if self.transportation.transporter_trip.transporter_content_object == user:
                return
        elif self.transportation.transporter_trip.transporter_content_type.model.lower() == 'vehicle':
            if self.transportation.transporter_trip.transporter_content_object.user == user:
                return
        raise ApiStandartException(dict(error_code=400056))

    def check_transfer_state(self, transportation_point_type):
        # transportation_point types: 0 - acceptance, 1 - delivery
        if transportation_point_type == 0:
            if self.status != 1 \
                    or self.acceptance_point_content_type.model.lower() == 'trusteetransportationpoint' \
                            and self.acceptance_point_content_object.is_completed:
                raise ApiStandartException(dict(error_code=400063))
        elif transportation_point_type == 1:
            if self.status != 3 \
                    or self.delivery_point_content_type.model.lower() == 'trusteetransportationpoint' \
                            and self.delivery_point_content_object.is_completed:
                raise ApiStandartException(dict(error_code=400064))

    def scan_update_statuses(self, transportation_point_type):
        transfer_point = None
        is_changed = False

        if transportation_point_type == 0:
            if self.acceptance_point_content_type.model.lower() == 'trusteetransportationpoint':
                transfer_point = self.acceptance_point_content_object.transfer_point
            if self.is_pending_status():
                self.set_picking_up_status()
                is_changed = True
        elif transportation_point_type == 1:
            if self.delivery_point_content_type.model.lower() == 'trusteetransportationpoint':
                transfer_point = self.delivery_point_content_object.transfer_point
            if self.is_on_board_status():
                self.set_transfer_status()
                is_changed = True

        transportation_transfer_point = TransportationTransferPoint.objects.get(
            transportation=self.transportation,
            transfer_point=transfer_point
        )

        if is_changed and not TransportationSchedule.objects.filter(
                transportation_transfer_point=transportation_transfer_point,
                type_check=1
        ).exists():
            TransportationSchedule.set_fact_arrival_datetime(transportation_transfer_point)
            if self.transportation.is_on_the_way_status():
                self.transportation.set_transfer_status()

    def transfer_update_statuses(self, transportation_point_type):
        transfer_point = None

        if transportation_point_type == 0:
            self.set_on_board_status()

            if self.acceptance_point_content_type.model.lower() == 'trusteetransportationpoint':
                transfer_point = self.acceptance_point_content_object.transfer_point

            if self.cargo_item_content_type.model.lower() == 'orderitem':
                if self.cargo_item_content_object.is_waiting_dispatch_status():
                    self.cargo_item_content_object.set_shipment_is_on_its_way_status()
        elif transportation_point_type == 1:
            self.set_delivered_status()

            if self.delivery_point_content_type.model.lower() == 'trusteetransportationpoint':
                transfer_point = self.delivery_point_content_object.transfer_point

            if not TransportationCargoItem.objects.filter(
                    cargo_item_object_id=self.cargo_item_content_type,
                    cargo_item_content_type=self.cargo_item_content_type,
                    status__in=[0, 1, 2, 3]
            ).exists():
                if self.cargo_item_content_type.model.lower() == 'orderitem':
                    self.cargo_item_content_object.set_delivered_status()
                    if not OrderItem.objects.filter(
                            order=self.cargo_item_content_object.order,
                            status__in=[0, 1, 2]
                    ).exists():
                        self.cargo_item_content_object.order.set_delivered_status()

        have_transfer = self.have_transfer_cargoes(transfer_point)

        if not have_transfer:
            transportation_transfer_point = TransportationTransferPoint.objects.get(
                transportation=self.transportation,
                transfer_point=transfer_point
            )
            if transportation_transfer_point.child:
                self.transportation.set_on_the_way_status()
            else:
                self.transportation.set_done_status()

            if transportation_point_type == 0:
                if self.acceptance_point_content_type.model.lower() == 'trusteetransportationpoint':
                    self.acceptance_point_content_object.set_completed()
            elif transportation_point_type == 1:
                if self.delivery_point_content_type.model.lower() == 'trusteetransportationpoint':
                    self.delivery_point_content_object.set_completed()

            TransportationSchedule.set_fact_depart_datetime(transportation_transfer_point)

    def set_transporter_characteristics(self, characteristics):
        for item in characteristics:
            try:
                cargo_item_characteristic = TransportationCargoItemCategoryCargoCharacteristic.objects.get(
                    transportation_cargo_item=self,
                    category_cargo_characteristic__characteristic=item['characteristic'],
                    category_cargo_characteristic__characteristic__is_main=True
                )
            except TransportationCargoItemCategoryCargoCharacteristic.DoesNotExist:
                continue
            cargo_item_characteristic.set_transporter_value(item['value'])

    def is_pending_status(self):
        if self.status == 0:
            return True
        return False

    def set_picking_up_status(self):
        self.status = 1
        self.save()

    def is_picking_up_status(self):
        if self.status == 1:
            return True
        return False

    def set_on_board_status(self):
        self.status = 2
        self.save()

    def is_on_board_status(self):
        if self.status == 2:
            return True
        return False

    def set_transfer_status(self):
        self.status = 3
        self.save()

    def is_transfer_status(self):
        if self.status == 3:
            return True
        return False

    def set_delivered_status(self):
        self.status = 4
        self.save()

    def is_delivered_status(self):
        if self.status == 4:
            return True
        return False

    def have_transfer_cargoes(self, transfer_point):
        cargo_items = list(self.transportation.transportation_cargo_items.all())
        for item in cargo_items:
            if item.is_picking_up_status() or item.is_transfer_status():
                if item.acceptance_point_content_type.model.lower() == 'trusteetransportationpoint' \
                        and item.acceptance_point_content_object.transfer_point == transfer_point:
                    return True

                elif item.delivery_point_content_type.model.lower() == 'trusteetransportationpoint' \
                        and item.delivery_point_content_object.transfer_point == transfer_point:
                    return True


class TransportationCargoItemCategoryCargoCharacteristic(Model):
    transportation_cargo_item = ForeignKey(TransportationCargoItem,
                                           related_name='transportation_cargo_item_characteristics',
                                           verbose_name=_(u'transportation order item'))
    category_cargo_characteristic = ForeignKey(
        CategoryCargoCharacteristic, related_name='characteristic_transportations_cargoes_items',
        verbose_name=_(u'category cargo characteristic'))
    stated_value = CharField(max_length=256, verbose_name=_(u'stated value'))
    transporter_value = CharField(max_length=256, blank=True, null=True, verbose_name=_(u'transporter_value'))

    created_at = DateTimeField(auto_now_add=True, verbose_name=_(u'created at'))

    class Meta:
        verbose_name = _(u'transportation cargo item category cargo characteristic')
        verbose_name_plural = _(u'transportation cargoes items category cargo characteristics')

    def __unicode__(self):
        return u'transportation cargo item - {0}, category cargo characteristic - {1}'.format(
            self.transportation_cargo_item.pk, self.category_cargo_characteristic.characteristic.title)

    def set_transporter_value(self, value):
        self.transporter_value = value
        self.save()
