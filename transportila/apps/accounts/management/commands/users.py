# -*- coding: utf-8 -*-
from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import Group
try:
    from django.contrib.auth import get_user_model
    User = get_user_model()
except ImportError:
    from django.contrib.auth.models import User, Group


def add():
    create_users()


def create_users():
    user, is_created = User.objects.get_or_create(email='fake@fake.com', defaults=dict(
        password=make_password('TransporT'),
        phone='7777777777',
        nickname='fake',
        is_staff=True,
        is_superuser=True,
        is_active=True
    ))

    if is_created:
        user.groups.add(Group.objects.get(pk=0))


