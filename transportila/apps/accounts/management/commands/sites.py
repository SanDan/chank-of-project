# -*- coding: utf-8 -*-
from django.db.utils import IntegrityError

from django.contrib.sites.models import Site

def add():
    s = Site.objects.get(id=1)
    s.name = 'transportila.com'
    s.domain = 'transportila.com'
    save_site(s)

def save_site(s):
    try:
        s.save()
    except IntegrityError:
        print 'Already in db: site --', s.name
