# coding=utf-8
from profiles.models import DocumentInfo
from vehicles.models import Vehicle, Cart


import re
import uuid

from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import Group
from django.utils.translation import ugettext_lazy as _
from rest_framework.serializers import EmailField, CharField, ValidationError, IntegerField, SerializerMethodField
from django.contrib.contenttypes.models import ContentType

# try:
#     from django.contrib.auth import get_user_model
#
#     User = get_user_model()
# except ImportError:
#     from django.contrib.auth.models import User

from accounts.models import UserConfirmation, Device, CustomUser
from support.serializers import CustomModelSerializer, CustomSerializer
from support.exceptions import ApiStandartException

__author__ = 'ustinov'


class RegistrationSerializer(CustomSerializer):
    email = EmailField(max_length=256)
    phone = CharField(max_length=32, required=False)
    password = CharField(max_length=64)
    nickname = CharField(max_length=128)
    user_group = IntegerField()

    def validate_email(self, email):
        email = email.lower()
        user = CustomUser.objects.filter(email=email).first()
        if user:
            user_confirmation = user.user_confirmation.filter(is_activated=False, confirmation_type=0).exists()
            if user_confirmation:
                user.send_email_for_registration()
                raise ApiStandartException(dict(error_code=400033))
            else:
                raise ValidationError(_(u'User with this email already exist.'))
        return email

    def validate_nickname(self, nickname):
        nickname = nickname.lower()
        if CustomUser.objects.filter(nickname=nickname).exists():
            raise ValidationError(_(u'User with this nickname already exist.'))
        return nickname

    def validate_password(self, password):
        pattern = "(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$"
        result = re.search(pattern, password)
        if not result:
            raise ValidationError(
                _(u'Password does not match the pattern.')
            )
        return password

    def validate_user_group(self, user_group):
        group_name = str(CustomUser.USERS_GROUPS[user_group][1])
        try:
            group = Group.objects.get(name=group_name)
        except Group.DoesNotExist:
            raise ValidationError(
                _(u'Group does not exist.')
            )
        return group

    def validate_phone(self, phone):
        if phone and CustomUser.objects.filter(phone=phone).exists():
            raise ValidationError(
                _(u'User with same phone already exist.')
            )
        return phone

    def save(self, validated_data):
        user = CustomUser.objects.create(
            email=validated_data.get('email'),
            password=make_password(validated_data.get('password')),
            phone=validated_data.get('phone'),
            nickname=validated_data.get('nickname')
        )

        user.groups.add(
            self.validated_data.get('user_group')
        )

        UserConfirmation.objects.create(user=user, key=uuid.uuid4(),
                                        confirmation_type=UserConfirmation.CONFIRMATION_TYPE[0][0])

        user.set_avatar(default=True)

        return user


class PasswordResetRequestSerializer(CustomSerializer):
    email = EmailField()

    def validate_email(self, email):
        try:
            user = CustomUser.objects.get(email=email.lower(), is_active=True)
        except CustomUser.DoesNotExist:
            raise ValidationError(_(u'User with email does not exist.'))
        return user


class ChangePasswordRequestSerializer(CustomSerializer):
    old_password = CharField(max_length=256)
    new_password = CharField(max_length=256)

    def validate_old_password(self, old_password):
        if not self.context['request'].user.check_password(old_password):
            raise ValidationError(_(u'Incorrect old password.'))
        return old_password

    def validate_new_password(self, new_password):
        pattern = "(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$"
        if not re.search(pattern, new_password):
            raise ValidationError(
                _(u'Password does not match the pattern.')
            )
        return new_password


class TokenAuthSerializer(CustomSerializer):
    grant_type = CharField()
    username = CharField(required=False)
    password = CharField(required=False)
    scope = CharField(required=False)
    refresh_token = CharField(required=False)
    client_id = CharField()
    client_secret = CharField()
    device_key = CharField(required=False)
    device_type = IntegerField(required=False)

    def validate_device_type(self, device_type):
        if not any(device_type in dev_type for dev_type in Device.DEVICES_TYPES):
            raise ValidationError(_(u'Check device type.'))
        return device_type

    def validate_username(self, username):
        return username.lower()


class UserSerializer(CustomModelSerializer):
    avatar = SerializerMethodField()
    rating = SerializerMethodField()

    def get_avatar(self, user):
        avatar = user.get_avatar()
        if avatar:
            return dict(url=avatar.url, id=avatar.id)
        else:
            return None

    def get_rating(self, user):
        return user.get_rating()

    class Meta:
        model = CustomUser
        fields = ('id', 'email', 'nickname', 'phone', 'avatar', 'rating')


class UserPublicInfoSerializer(CustomModelSerializer):
    avatar = SerializerMethodField()
    first_name = SerializerMethodField()
    last_name = SerializerMethodField()
    passport_id = SerializerMethodField()

    def get_first_name(self, user):
        if self.basic_info:
            return self.basic_info.first_name
        return None

    def get_last_name(self, user):
        if self.basic_info:
            return self.basic_info.last_name
        return None

    def get_avatar(self, user):
        avatar = user.get_avatar()
        if avatar:
            return dict(url=avatar.url, id=avatar.id)
        else:
            return None

    def get_passport_id(self, user):
        if self.document_info:
            return self.document_info.passport_id
        return None

    class Meta:
        model = CustomUser
        fields = ('id', 'nickname', 'first_name', 'last_name', 'avatar', 'passport_id')

    def __init__(self, *args, **kwargs):
        super(UserPublicInfoSerializer, self).__init__(*args, **kwargs)
        self.basic_info = self.instance.basic_info_profiles.first()
        self.document_info = self.instance.user_document_info.first()


