# coding=utf-8
from django.db.models import Q

import json

from django.http import HttpResponse
from oauth2_provider.views.mixins import OAuthLibMixin
from oauth2_provider.settings import oauth2_settings
from oauthlib.oauth2 import Server
from braces.views import CsrfExemptMixin
from rest_framework.viewsets import ModelViewSet
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.response import Response
from oauth2_provider.ext.rest_framework import TokenHasReadWriteScope
from rest_framework import permissions
from django.shortcuts import get_object_or_404
try:
    from django.contrib.auth import get_user_model
    User = get_user_model()
except ImportError:
    from django.contrib.auth.models import User
from rest_framework.generics import GenericAPIView

from .serializers import RegistrationSerializer, PasswordResetRequestSerializer, ChangePasswordRequestSerializer, \
    TokenAuthSerializer, UserSerializer
from .models import UserConfirmation, CustomUser
from profiles.serializers import ProfileSerializer
from queue.send_email import SendEmail
from django.conf import settings

__author__ = 'ustinov'


class RegistrationView(APIView):
    permission_classes = ()

    def post(self, *args, **kwargs):
        serializer = RegistrationSerializer(data=self.request.data)
        serializer.is_valid()
        user = serializer.save(serializer.validated_data)
        user.send_email_for_registration()
        return Response(status=status.HTTP_201_CREATED)


class RegistrationConfirmView(APIView):
    permission_classes = ()

    def get(self, request, *args, **kwargs):
        user_confirmation = get_object_or_404(UserConfirmation, key=kwargs['key'], is_activated=False)
        user_confirmation.user.activate()
        user_confirmation.activate()
        return Response(status=status.HTTP_200_OK)


class TokenView(CsrfExemptMixin, OAuthLibMixin, GenericAPIView):
    """
    Implements an endpoint to provide access tokens

    The endpoint is used in the following flows:
    * Authorization code
    * Password
    * Client credentials
    """
    server_class = Server
    validator_class = oauth2_settings.OAUTH2_VALIDATOR_CLASS
    oauthlib_backend_class = oauth2_settings.OAUTH2_BACKEND_CLASS
    permission_classes = ()

    def add_custom_response_params(self, body_params, user):
        avatar = user.get_avatar()
        body_params['profile'] = dict(
            id=user.id,
            user_group=(lambda group: group.id if group is not None else None)(user.groups.first()),
            email=user.email,
            nickname=user.nickname,
            avatar=dict(url=avatar.url, id=avatar.id) if avatar else None
        )
        return json.dumps(body_params)

    def post(self, request, *args, **kwargs):
        serializer = TokenAuthSerializer(data=request.data)
        serializer.initial_data['client_secret'] = settings.TRANSPORTILA_CLIENT_SECRET
        serializer.initial_data['client_id'] = settings.TRANSPORTILA_CLIENT_ID
        serializer.is_valid()
        if serializer.validated_data.get('grant_type') == 'password':
            user = User.check_user(serializer.validated_data.get('username'),
                                   serializer.validated_data.get('password'), 'email')
        request.POST = serializer.data  # oauth provider use request POST
        url, headers, body, response_status = self.create_token_response(request)
        if response_status == 200 and serializer.validated_data.get('grant_type') == 'password':
            body_params = json.loads(body)
            body = self.add_custom_response_params(body_params, user)

            user.clear_tokens(body_params['access_token'])
            user.update_device(serializer.validated_data.get('device_key'),
                               serializer.validated_data.get('device_type'))

        response = HttpResponse(content=body, status=response_status)

        for k, v in headers.items():
            response[k] = v
        return response


class RevokeTokenView(CsrfExemptMixin, OAuthLibMixin, APIView):
    """
    Implements an endpoint to revoke access or refresh tokens
    """
    server_class = Server
    validator_class = oauth2_settings.OAUTH2_VALIDATOR_CLASS
    oauthlib_backend_class = oauth2_settings.OAUTH2_BACKEND_CLASS
    permission_classes = ()

    def post(self, request, *args, **kwargs):
        request.data['client_id'] = settings.TRANSPORTILA_CLIENT_ID
        request.data['client_secret'] = settings.TRANSPORTILA_CLIENT_SECRET
        request.POST = request.data
        url, headers, body, status = self.create_revocation_response(request)
        response = HttpResponse(content=body or '', status=status)

        for k, v in headers.items():
            response[k] = v
        return response


class ResetPasswordView(APIView):
    permission_classes = ()

    def post(self, request, *args, **kwargs):
        serializer = PasswordResetRequestSerializer(data=request.data)
        serializer.is_valid()
        user = serializer.validated_data.get('email')
        user.reset_password()
        return Response(status=status.HTTP_200_OK)


class ActivatePasswordView(APIView):
    permission_classes = ()

    def get(self, request, *args, **kwargs):
        user_confirmation = get_object_or_404(UserConfirmation, key=kwargs['key'], is_activated=False)
        user_confirmation.activate()
        user_confirmation.user.set_hash_password(user_confirmation.newuserpassword.password)
        return Response(status=status.HTTP_200_OK)


class ChangePasswordView(APIView):
    permission_classes = (permissions.IsAuthenticated, TokenHasReadWriteScope)

    def post(self, request, *args, **kwargs):
        serializer = ChangePasswordRequestSerializer(data=request.data, context=dict(request=request))
        serializer.is_valid()
        request.user.set_password(serializer.validated_data.get('new_password'))
        request.user.save()
        SendEmail.send.delay(
            dict(
                data=dict(
                    subject_template=settings.CHANGE_PASSWORD_LETTER_SUBJECT,
                    template_text=settings.CHANGE_PASSWORD_LETTER_TXT,
                    template_html=settings.CHANGE_PASSWORD_LETTER_HTML,
                    from_email=settings.DEFAULT_FROM_EMAIL,
                    emails_to=[request.user.email],
                    params=dict(
                        file_server_url=settings.FILE_SERVER_URL,
                        user=request.user
                    )
                )
            )
        )
        return Response(status=status.HTTP_200_OK)


class CustomUserViewSet(ModelViewSet):
    model = CustomUser
    serializer_class = UserSerializer
    permission_classes = (permissions.IsAuthenticated, )
    queryset = CustomUser.objects.all()

    def retrieve(self, request, *args, **kwargs):
        return Response(ProfileSerializer(self.get_object()).data)

    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        if 'pattern' in self.request.query_params:
            queryset = queryset.filter(
                Q(email__icontains=self.request.query_params['pattern']) |
                Q(phone__icontains=self.request.query_params['pattern']) |
                Q(nickname__icontains=self.request.query_params['pattern'])
            )
        else:
            queryset = queryset[:50]
        return Response(self.serializer_class(queryset, many=True).data)


