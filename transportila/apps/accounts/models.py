# coding=utf-8

from pytz import timezone
from datetime import datetime
import uuid

from django.conf import settings
from django.contrib.auth.hashers import make_password
from strgen import StringGenerator as SG
from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from django.utils.translation import ugettext_lazy as _
from oauth2_provider.models import AccessToken
from django.contrib.contenttypes.models import ContentType

from queue.send_email import SendEmail
from support.exceptions import ApiStandartException

__author__ = 'sandan'


class CustomUserManager(BaseUserManager):
    def create_user(self, password=None):
        """
        Creates and saves a User with the given password.
        """
        user = self.model(
            is_staff=False,
            is_active=True,
            is_superuser=False,
            date_joined=datetime.utcnow().replace(tzinfo=timezone('UTC'))
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, password):
        """
        Creates and saves a superuser with the given password.
        """
        user = self.model(
            password=password,
            is_staff=True,
            is_active=True,
            is_superuser=True,
            date_joined=datetime.utcnow().replace(tzinfo=timezone('UTC'))
        )
        user.save(using=self._db)
        return user


class CustomUser(AbstractBaseUser, PermissionsMixin):
    USERS_GROUPS = (
        (0, u'customer'),
        (1, u'transporter'),
        (2, u'dispatcher'),
    )

    email = models.EmailField(unique=True, db_index=True, max_length=256, verbose_name=_(u'email'))
    nickname = models.CharField(unique=True, db_index=True, blank=True, null=True, max_length=128,
                                verbose_name=_(u'nickname'))
    phone = models.CharField(max_length=32, null=True, blank=True, verbose_name=_(u'phone number'))

    is_staff = models.BooleanField(default=False, verbose_name=_(u'admin'))
    date_joined = models.DateTimeField(default=datetime.utcnow, verbose_name=_(u'created at'))

    is_active = models.BooleanField(default=False, verbose_name=_(u'is active'))

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['nickname']

    objects = CustomUserManager()

    class Meta:
        verbose_name = _(u'user')
        verbose_name_plural = _(u'users')

    def __unicode__(self):
        return self.get_username()

    def get_username(self):
        return getattr(self, self.USERNAME_FIELD)

    def get_full_name(self):
        return "%s" % self.email

    def get_short_name(self):
        return "%s" % self.email

    def activate(self):
        self.is_active = True
        self.save()

    def get_avatar(self):
        from auxiliaries.models import Attachment
        try:
            return Attachment.objects.get(content_type=ContentType.objects.get_for_model(self), object_id=self.id)
        except Attachment.DoesNotExist:
            return None

    def reset_password(self):
        confirmations = UserConfirmation.objects.filter(confirmation_type=1, is_activated=False, user=self)
        for item in confirmations:
            item.delete()

        password = SG('[\l\d]{8}').render()
        user_confirmation = UserConfirmation.objects.create(
            user=self,
            key=uuid.uuid4(),
            confirmation_type=UserConfirmation.CONFIRMATION_TYPE[1][0]
        )
        NewUserPassword.objects.create(password=make_password(password), user_confirmation=user_confirmation)
        SendEmail.send.delay(
            dict(
                data=dict(
                    subject_template=settings.RESET_PASSWORD_LETTER_SUBJECT,
                    template_text=settings.RESET_PASSWORD_LETTER_TXT,
                    template_html=settings.RESET_PASSWORD_LETTER_HTML,
                    from_email=settings.DEFAULT_FROM_EMAIL,
                    emails_to=[self.email],
                    params=dict(
                        activation_url=settings.FRONT_ACTIVATE_PASSWORD_URL.format(
                            user_confirmation.key
                        ),
                        password=password,
                        file_server_url=settings.FILE_SERVER_URL,
                        user=self
                    )
                )
            ))

    def get_user_group(self):
        return self.groups.all()[0].id

    def set_hash_password(self, hash_password):
        self.password = hash_password
        self.save()

    def clear_tokens(self, current_token):
        for token in AccessToken.objects.filter(user=self).exclude(token=current_token):
            token.delete()

    def update_device(self, device_key, device_type):
        if device_key and device_type is not None:
            for device in list(Device.objects.filter(type=device_type, key=device_key)):
                device.delete()
            device, created = Device.objects.get_or_create(user=self, defaults=dict(type=device_type, key=device_key))
            if not created:
                device.type = device_type
                device.key = device_key
                device.save()

    @classmethod
    def check_user(cls, username, password, by_field):
        try:
            user = eval('cls.objects.get({0}=username)'.format(by_field))
        except cls.DoesNotExist:
            raise ApiStandartException(dict(error_code=400002))
        if not user.check_password(password):
            raise ApiStandartException(dict(error_code=400004))
        if not user.is_active:
            raise ApiStandartException(dict(error_code=400003))
        user_confirmation = user.user_confirmation.filter(is_activated=False, confirmation_type=0).exists()
        if user_confirmation:
            user.send_email_for_registration()
            raise ApiStandartException(dict(error_code=400022))
        return user

    def check_nickname(self, nickname):
        user = CustomUser.objects.filter(nickname=nickname).first()
        if user and user != self:
            raise ApiStandartException(dict(error_code=400018))

    def set_avatar(self, default=False, avatar_url=None):
        from auxiliaries.models import Attachment

        old_avatar = self.get_avatar()
        if old_avatar:
            old_avatar.delete()
        if default:
            avatar_url = self.get_default_avatar_url(self.groups.first())
        Attachment.objects.create(
            content_type=ContentType.objects.get_for_model(self),
            object_id=self.id,
            url=avatar_url,
            user=self
        )

    @staticmethod
    def get_default_avatar_url(user_group):
        if user_group.id == 0:
            return 'https://s3.amazonaws.com/transportila2/images/1438936079147/1438936079.jpg'
        elif user_group.id == 1:
            return 'https://s3.amazonaws.com/transportila2/images/1438936288116/1438936288.jpg'
        elif user_group.id == 2:
            return 'https://s3.amazonaws.com/transportila2/images/1438936350683/1438936350.jpg'

    def send_email_for_registration(self):
        SendEmail.send.delay(
            dict(
                data=dict(
                    subject_template=settings.CONFIRM_REGISTRATION_LETTER_SUBJECT,
                    template_text=settings.CONFIRM_REGISTRATION_LETTER_TXT,
                    template_html=settings.CONFIRM_REGISTRATION_LETTER_HTML,
                    from_email=settings.DEFAULT_FROM_EMAIL,
                    emails_to=[self.email],
                    params=dict(
                        activation_url=settings.FRONT_CONFIRM_REGISTRATION_URL.format(
                            self.user_confirmation.filter(is_activated=False).first().key
                        ),
                        file_server_url=settings.FILE_SERVER_URL,
                        user=self
                    )
                )
            ))

    def check_order_limited_access(self, order):
        from orders.models import Bet
        if order.customer == self or CustomUser.objects.filter(
                b_orders__order_bets__status=Bet.BETS_STATUSES[2][0], b_orders__order=order).exists():
            return False
        return True

    def is_customer(self):
        groups = self.groups.all().values('name')
        if groups and groups[0]['name'] == 'customer':
            return True
        return False

    def is_transporter(self):
        groups = self.groups.all().values('name')
        if groups and groups[0]['name'] == 'transporter':
            return True
        return False

    def is_dispatcher(self):
        groups = self.groups.all().values('name')
        if groups and groups[0]['name'] == 'dispatcher':
            return True
        return False

    def get_rating(self):
        reviews = list(self.reviews.all())
        if len(reviews) > 0:
            count_good_marks = 0
            for item in reviews:
                if item.mark == 1:
                    count_good_marks += 1
            return int(count_good_marks / float(len(reviews)) * 100)
        else:
            return 0


class UserConfirmation(models.Model):
    CONFIRMATION_TYPE = (
        (0, _(u'registration')),
        (1, _(u'password'))
    )
    key = models.UUIDField(primary_key=True, db_index=True, unique=True, verbose_name=_(u'key'))
    user = models.ForeignKey(CustomUser, related_name='user_confirmation', verbose_name=_(u'user'))
    is_activated = models.BooleanField(default=False, verbose_name=_(u'is activated'))
    confirmation_type = models.PositiveSmallIntegerField(choices=CONFIRMATION_TYPE,
                                                         verbose_name=_(u'confirmation type'))
    created_at = models.DateTimeField(auto_now_add=True, verbose_name=_(u'created at'))
    confirmed_at = models.DateTimeField(blank=True, null=True, verbose_name=_(u'confirmed at'))

    class Meta:
        verbose_name = _(u'user confirmation')
        verbose_name_plural = _(u'user confirmation')

    def __unicode__(self):
        return u'user confirmation key - {0}'.format(self.key)

    def activate(self):
        self.is_activated = True
        self.confirmed_at = datetime.utcnow()
        self.save()


class NewUserPassword(models.Model):
    password = models.CharField(max_length=512, verbose_name=_(u'password'))
    user_confirmation = models.OneToOneField(UserConfirmation, verbose_name=_(u'user confirmation'))

    class Meta:
        verbose_name = _(u'new user password')
        verbose_name_plural = _(u'new users passwords')

    def __unicode__(self):
        return u'new user password confirmation'


class Device(models.Model):
    DEVICES_TYPES = (
        (0, 'iOS'),
        (1, 'Android'),
        (2, 'Windows Phone')
    )

    key = models.CharField(max_length=512, verbose_name=_(u'device key'))
    type = models.PositiveSmallIntegerField(choices=DEVICES_TYPES, verbose_name=_(u'device type'))
    user = models.OneToOneField(CustomUser, unique=True, primary_key=True,  verbose_name=_(u'user'))

    def __unicode__(self):
        return u'device {0}'.format(self.key)

    class Meta:
        verbose_name = _(u'device')
        verbose_name_plural = _(u'devices')

