from django.conf.urls import patterns, url, include
from rest_framework import routers

from .api import CategoryCargoViewSet, OrderViewSet, BetsViewSet, ResendCertificateView

__author__ = 'sandan'

v1_router = routers.SimpleRouter()
v1_router.register(r'orders/categories-cargo', CategoryCargoViewSet)
v1_router.register(r'orders', OrderViewSet)
v1_router.register(r'bets', BetsViewSet)

v1_urlpatterns = [
    url(r'', include(v1_router.urls)),
    url(r'orders/certificates/resend/$', ResendCertificateView.as_view()),
]


urlpatterns = [
    url(r'^api/v1/', include(v1_urlpatterns)),
]
