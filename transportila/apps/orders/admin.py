from django.contrib.admin import ModelAdmin, TabularInline
from django.contrib.admin.decorators import register
from grappelli.forms import GrappelliSortableHiddenMixin
from modeltranslation.admin import TranslationAdmin, TranslationTabularInline
from grappelli_nested.admin import NestedTabularInline, NestedStackedInline, NestedModelAdmin

from .models import Trustee, TransportationType, CategoryCargo, CategoryCargoCharacteristic, Order, OrderBettor, Bet, \
    OrderDate, OrderItem, OrderItemCategoryCargoCharacteristic, CategoryCargoTransportationType, OrderItemPoint


@register(Trustee)
class AdminTrustee(ModelAdmin):
    list_display = ['email', 'id', 'first_name', 'last_name', 'social_id', 'created_at']


@register(TransportationType)
class AdminTransportationType(TranslationAdmin):
    prepopulated_fields = {'slug': ('title',)}
    list_display = ['slug', 'title']


class InlineCategoryCargoTransportationType(NestedTabularInline):
    model = CategoryCargoTransportationType
    extra = 0


class InlineCategoryCargoCharacteristic(GrappelliSortableHiddenMixin, NestedTabularInline):
    model = CategoryCargoCharacteristic
    extra = 0
    sortable_field_name = 'position'


class InlineCategoryCargo(GrappelliSortableHiddenMixin, TranslationTabularInline,  NestedStackedInline):
    fieldsets = (
        (
            None,
            {
                'fields': ('slug', 'title', 'position')
            }
        ),
    )

    model = CategoryCargo
    extra = 0
    sortable_field_name = 'position'

    inlines = [InlineCategoryCargoTransportationType, InlineCategoryCargoCharacteristic]

    def get_queryset(self, request):
        category_slug = request.path.split('/')[-2]
        return super(InlineCategoryCargo, self).get_queryset(request).filter(parent_id=category_slug)


@register(CategoryCargo)
class AdminCategoryCargo(TranslationAdmin, NestedModelAdmin):
    prepopulated_fields = {'slug': ('title',)}
    list_display = ['slug', 'title', 'parent', 'position', 'created_at']
    list_editable = ['position']
    inlines = [InlineCategoryCargo]

    def get_queryset(self, request):
        return super(AdminCategoryCargo, self).get_queryset(request).filter(parent=None)


@register(CategoryCargoCharacteristic)
class AdminCategoryCargoCharacteristic(ModelAdmin):
    list_display = ['category_cargo', 'characteristic']


class AdminInlineOrderItem(TabularInline):
    model = OrderItem
    extra = 1


@register(Order)
class AdminOrder(ModelAdmin):
    list_display = ['uuid', 'title', 'status', 'customer', 'category_cargo_transportation_type', 'order_type', 'valid_until', 'date_completed',
                    'date_published', 'distance']
    inlines = [AdminInlineOrderItem]


@register(OrderBettor)
class AdminOrderBettor(ModelAdmin):
    pass


@register(Bet)
class AdminBet(ModelAdmin):
    list_display = ['id', 'order_bettor', 'status', 'cost', 'acceptance_at', 'created_at']


@register(OrderDate)
class AdminOrderDate(ModelAdmin):
    list_display = ['id', 'first_date', 'second_date', 'order_date_type']


@register(OrderItem)
class AdminOrderItem(ModelAdmin):
    list_display = ['uuid', 'title', 'order', 'status', 'created_at']


@register(OrderItemPoint)
class AdminOrderItemPoint(ModelAdmin):
    list_display = ['id', 'date_transfer']


@register(OrderItemCategoryCargoCharacteristic)
class AdminOrderItemCategoryCargoCharacteristic(ModelAdmin):
    list_display = ['id', 'order_item', 'category_cargo_characteristic', 'value']

