from modeltranslation.translator import register, TranslationOptions
from .models import TransportationType, CategoryCargo

__author__ = 'sandan'


@register(TransportationType)
class CharacteristicTranslationOptions(TranslationOptions):
    fields = ('title',)


@register(CategoryCargo)
class CharacteristicUnitTranslationOptions(TranslationOptions):
    fields = ('title',)
