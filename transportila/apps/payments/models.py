from django.db.models import Model, FloatField, PositiveIntegerField, ForeignKey, SlugField, CharField
from django.utils.translation import ugettext_lazy as _


class Currency(Model):
    slug = SlugField(max_length=256, primary_key=True, db_index=True, unique=True, verbose_name=_(u'slug'))
    title = CharField(max_length=256, blank=True, null=True, verbose_name=_(u'title'))

    class Meta:
        verbose_name = _(u'currency')
        verbose_name_plural = _(u'currencies')

    def __unicode__(self):
        return u'currency {0}'.format(self.slug)


class OrderPaymentInfo(Model):
    PAYMENT_METHOD_TYPE = (
        (0, _(u'paypall')),
        (1, _(u'wire transfer')),
        (2, _(u'cash'))
    )

    cost_transportation = FloatField(verbose_name=_(u'cost transportation'))
    pre_payment = FloatField(verbose_name=_(u'pre payment'))
    post_payment = FloatField(verbose_name=_(u'post payment'))
    payment_method_type = PositiveIntegerField(choices=PAYMENT_METHOD_TYPE, verbose_name=_(u'payment method type'))
    currency = ForeignKey(Currency, verbose_name=_(u'currency'))

    class Meta:
        verbose_name = _(u'payment info')
        verbose_name_plural = _(u'payment info')

    def __unicode__(self):
        return u'payment info {0}'.format(self.id)
