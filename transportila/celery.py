from __future__ import absolute_import

import os

from celery import Celery


__author__ = 'vol'

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'transportila.settings')

from django.conf import settings

app = Celery('transportila')
app.config_from_object('django.conf:settings')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)